package com.dana.batch2.danatopupapp.ui.user

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserBalanceResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserProfileResponse
import com.dana.batch2.danatopupapp.utils.authToken
import com.dana.batch2.danatopupapp.utils.getUserRequestTest
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class UserViewModelTest {
    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerUserProfile: Observer<ApiResponse<UserProfileResponse>>

    @Mock
    private lateinit var observerHomeFragment: Observer<ApiResponse<UserBalanceResponse>>

    private lateinit var userViewModel: UserViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@UserViewModelTest)
        userViewModel = UserViewModel(mainRepository)
    }



    @Test
    fun getUserProfile() {
        val userProfile = ApiResponse.success(getUserRequestTest())
        val userProfileLive = MutableLiveData<ApiResponse<UserProfileResponse>>()
        userProfileLive.value = userProfile

        Mockito.`when`(mainRepository.getUserProfile(authToken)).thenReturn(userProfileLive)

        userViewModel.setAuthToken(authToken)
        userViewModel.getUserProfile().observeForever(observerUserProfile)

        verify(observerUserProfile).onChanged(userProfile)

    }


    @Test
    fun getUserBalance() {
        val userBalance = ApiResponse.success(UserBalanceResponse(3720000F))
        val userBalanceLive = MutableLiveData<ApiResponse<UserBalanceResponse>>()
        userBalanceLive.value = userBalance

        Mockito.`when`(mainRepository.getUserBalance(authToken)).thenReturn(userBalanceLive)

        userViewModel.setAuthToken(authToken)
        userViewModel.getUserBalance().observeForever(observerHomeFragment)

        verify(observerHomeFragment).onChanged(userBalance)
    }
}