package com.dana.batch2.danatopupapp.ui.confirmreceipt

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.UploadTransferRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.NonBodyResponse
import com.dana.batch2.danatopupapp.utils.authToken
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.io.File

class ConfirmReceiptViewModelTest {

    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerUploadTransfer: Observer<ApiResponse<NonBodyResponse>>

    private lateinit var confirmReceiptViewModel: ConfirmReceiptViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@ConfirmReceiptViewModelTest)
        confirmReceiptViewModel = ConfirmReceiptViewModel(mainRepository)
    }

    @Test
    fun processUploadReceipt() {
        val uploadTransfer = UploadTransferRequest(File("Path"))
        val nonBodyResponse = ApiResponse.success(NonBodyResponse("success", 200))
        val paymentToken = "80006289653399715"
        val uploadTransferLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        uploadTransferLive.value = nonBodyResponse

        Mockito.`when`(mainRepository.postTransferReceipt(authToken, uploadTransfer, paymentToken))
            .thenReturn(uploadTransferLive)

        confirmReceiptViewModel.setPaymentToken(authToken)
        confirmReceiptViewModel.setPaymentToken(paymentToken)
        confirmReceiptViewModel.processUploadReceipt(uploadTransfer).observeForever(observerUploadTransfer)

        verify(observerUploadTransfer).onChanged(nonBodyResponse)
    }
}