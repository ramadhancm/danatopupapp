package com.dana.batch2.danatopupapp.ui.topup

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.BalanceCatalogResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserProfileResponse
import com.dana.batch2.danatopupapp.utils.authToken
import com.dana.batch2.danatopupapp.utils.getUserRequestTest
import com.dana.batch2.danatopupapp.utils.provideBalanceCatalogTest
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TopUpViewModelTest {


    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerUserProfile: Observer<ApiResponse<UserProfileResponse>>

    @Mock
    private lateinit var observerBalanceCatalog: Observer<ApiResponse<List<BalanceCatalogResponse>>>

    private lateinit var topUpViewModel: TopUpViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@TopUpViewModelTest)
        topUpViewModel= TopUpViewModel(mainRepository)
    }


    @Test
    fun getUserProfile() {
        val userProfile = ApiResponse.success(getUserRequestTest())
        val userProfileLive = MutableLiveData<ApiResponse<UserProfileResponse>>()
        userProfileLive.value = userProfile

        Mockito.`when`(mainRepository.getUserProfile(authToken)).thenReturn(userProfileLive)

        topUpViewModel.setAuthToken(authToken)
        topUpViewModel.getUserProfile().observeForever(observerUserProfile)

        verify(observerUserProfile).onChanged(userProfile)

    }

    @Test
    fun getBalanceCatalog() {
        val balanceCatalogs = ApiResponse.success(provideBalanceCatalogTest())
        val balanceCatalogLive = MutableLiveData<ApiResponse<List<BalanceCatalogResponse>>>()
        balanceCatalogLive.value = balanceCatalogs

        Mockito.`when`(mainRepository.getCatalogBalance(authToken)).thenReturn(balanceCatalogLive)

        topUpViewModel.setAuthToken(authToken)
        topUpViewModel.getBalanceCatalog().observeForever(observerBalanceCatalog)

        verify(observerBalanceCatalog).onChanged(balanceCatalogs)
    }
}