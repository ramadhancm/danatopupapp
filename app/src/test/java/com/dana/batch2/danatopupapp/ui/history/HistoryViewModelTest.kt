package com.dana.batch2.danatopupapp.ui.history

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpHistoryResponse
import com.dana.batch2.danatopupapp.utils.authToken
import com.dana.batch2.danatopupapp.utils.provideTopUpHistory
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class HistoryViewModelTest {

    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerHistory: Observer<ApiResponse<List<TopUpHistoryResponse>>>

    private lateinit var historyViewModel: HistoryViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@HistoryViewModelTest)
        historyViewModel = HistoryViewModel(mainRepository)
    }

    @Test
    fun getTopUpHistory() {
        val topUpHistory = ApiResponse.success(provideTopUpHistory())
        val topUpHistoryLive = MutableLiveData<ApiResponse<List<TopUpHistoryResponse>>>()
        topUpHistoryLive.value = topUpHistory

        Mockito.`when`(mainRepository.getTopUpHistory(authToken)).thenReturn(topUpHistoryLive)

        historyViewModel.setAuthToken(authToken)
        historyViewModel.getTopUpHistory().observeForever(observerHistory)

        verify(observerHistory).onChanged(topUpHistory)
    }
}