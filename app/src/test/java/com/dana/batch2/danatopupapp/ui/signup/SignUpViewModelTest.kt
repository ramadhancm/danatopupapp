package com.dana.batch2.danatopupapp.ui.signup

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.NonBodyResponse
import com.dana.batch2.danatopupapp.utils.provideSignUpRequest
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SignUpViewModelTest {

    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerNonBodyResponse: Observer<ApiResponse<NonBodyResponse>>

    private lateinit var signUpViewModel: SignUpViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@SignUpViewModelTest)
        signUpViewModel = SignUpViewModel(mainRepository)
    }

    @Test
    fun processSignUp() {
        val signUpRequest = provideSignUpRequest()
        val signUpResponse = ApiResponse.success(NonBodyResponse("success", 200))
        val signUpResponseLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        signUpResponseLive.value = signUpResponse

        Mockito.`when`(mainRepository.postSignUp(signUpRequest)).thenReturn(signUpResponseLive)

        signUpViewModel.processSignUp(signUpRequest).observeForever(observerNonBodyResponse)

        verify(observerNonBodyResponse).onChanged(signUpResponse)
    }
}