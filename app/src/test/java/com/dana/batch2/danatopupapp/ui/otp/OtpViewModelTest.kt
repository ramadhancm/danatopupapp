package com.dana.batch2.danatopupapp.ui.otp

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.SendOtpRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.VerifyOtpRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.NonBodyResponse
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class OtpViewModelTest {


    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerOtpResponse: Observer<ApiResponse<NonBodyResponse>>

    private lateinit var otpViewModel: OtpViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@OtpViewModelTest)
        otpViewModel = OtpViewModel(mainRepository)
    }

    @Test
    fun postSendOtp() {
        val sendOtpRequest = SendOtpRequest("6289653399715", "rizkyalfikri@gmail.com")
        val sendOtpResponse = ApiResponse.success(NonBodyResponse("success", 200))
        val sendOtpResponseLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        sendOtpResponseLive.value = sendOtpResponse

        Mockito.`when`(mainRepository.postSendOtp(sendOtpRequest)).thenReturn(sendOtpResponseLive)

        otpViewModel.postSendOtp(sendOtpRequest).observeForever(observerOtpResponse)

        verify(observerOtpResponse).onChanged(sendOtpResponse)

    }

    @Test
    fun postVerifyOtp() {
        val verifyOtpRequest = VerifyOtpRequest("6289653399715", 123456)
        val verifyOtpResponse =
            ApiResponse.success( NonBodyResponse("success", 200))
        val verifyOtpResponseLive = MutableLiveData<ApiResponse<NonBodyResponse>>()
        verifyOtpResponseLive.value = verifyOtpResponse

        Mockito.`when`(mainRepository.postVerifyOtp(verifyOtpRequest))
            .thenReturn(verifyOtpResponseLive)

        otpViewModel.postVerifyOtp(verifyOtpRequest).observeForever(observerOtpResponse)

        verify(observerOtpResponse).onChanged(verifyOtpResponse)
    }
}