package com.dana.batch2.danatopupapp.ui.confirmqrcode

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserProfileResponse
import com.dana.batch2.danatopupapp.utils.authToken
import com.dana.batch2.danatopupapp.utils.getUserRequestTest
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class ConfirmQrCodeViewModelTest {

    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerUserProfile: Observer<ApiResponse<UserProfileResponse>>

    private lateinit var confirmQrCodeViewModel: ConfirmQrCodeViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@ConfirmQrCodeViewModelTest)
        confirmQrCodeViewModel = ConfirmQrCodeViewModel(mainRepository)
    }

    @Test
    fun getUserProfile() {
        val userProfile = ApiResponse.success(getUserRequestTest())
        val userProfileLive = MutableLiveData<ApiResponse<UserProfileResponse>>()
        userProfileLive.value = userProfile

        Mockito.`when`(mainRepository.getUserProfile(authToken)).thenReturn(userProfileLive)

        confirmQrCodeViewModel.setAuthToken(authToken)
        confirmQrCodeViewModel.getUserProfile().observeForever(observerUserProfile)

        verify(observerUserProfile).onChanged(userProfile)

    }
}