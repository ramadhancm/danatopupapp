package com.dana.batch2.danatopupapp.utils

import com.dana.batch2.danatopupapp.data.source.remote.request.SignUpRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.TopUpBalanceRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.*

const val authToken =
    "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2Mjg5NjUzMzk5NzE1IiwiZXhwIjoxNTg5NjU2OTIzLCJpYXQiOjE1ODk2Mzg5MjN9.CU03Yi9ruoA_NgIhHHwI-PzEMtGTgLBsXiz77oYsRIRECzkZhEUTdRtPlVNkQuNZj19GP5tz1t_oeV4F5k8ocw"

fun getUserRequestTest(): UserProfileResponse {
    return UserProfileResponse(
        "rizkyalfikri00@gmail.com",
        "rizky",
        "Hore Hore",
        "6289653399715"
    )

}

fun provideBalanceCatalogTest(): List<BalanceCatalogResponse> {
    return listOf(
        BalanceCatalogResponse(
            1,
            "e-10",
            10000
        ),
        BalanceCatalogResponse(
            1,
            "e-10",
            10000
        ),

        BalanceCatalogResponse(
            2,
            "e-20",
            20000
        ),
        BalanceCatalogResponse(
            3,
            "e-50",
            50000
        ),
        BalanceCatalogResponse(
            4,
            "e-100",
            100000
        ), BalanceCatalogResponse(
            5,
            "e-200",
            200000
        ),
        BalanceCatalogResponse(
            6,
            "e-500",
            200000
        )
    )
}

fun provideTopUpHistory(): List<TopUpHistoryResponse> {
    return listOf(
        TopUpHistoryResponse(
            13, 53, 10000F, 1, "BCA",
            "80006289653399715", 1,
            "http://ff4f6802.ngrok.io/downloadFile/46f56176-35f8-4b22-88f4-24ffca72eb7b.png",
            "2020-05-16 15:38:57"
        ),
        TopUpHistoryResponse(
            14, 53, 50000F, 1, "BCA",
            "80006289653399715", 1,
            "http://ff4f6802.ngrok.io/downloadFile/46f56176-35f8-4b22-88f4-24ffca72eb7b.png",
            "2020-05-16 15:38:57"
        ),
        TopUpHistoryResponse(
            15, 53, 50000F, 2, "Indomaret",
            "80006289653399715", 1,
            "http://ff4f6802.ngrok.io/downloadFile/46f56176-35f8-4b22-88f4-24ffca72eb7b.png",
            "2020-05-16 15:38:57"
        )
    )
}

fun provideTopUpRequest() = TopUpBalanceRequest("6289653399715", "e-10", 1)

fun provideLoginResponse() = LoginResponse("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2Mjg5NjUzMzk5NzE1IiwiZXhwIjoxNTg5NjU2ODczLCJpYXQiOjE1ODk2Mzg4NzN9.EXi1tWRV2A1b-Jcc7N_3QowD3Ab65dd1RSihWI1ItI_pDcqhFEfkN2W9wMefazwioNq-zVPPqo24a08TtWgL0g")

fun provideSignUpRequest() = SignUpRequest(
    "rizkyalfikri@dana.id",
    "rizky",
    "alfikri",
    "Qwerty00@@",
    "628965339715"
)

fun provideTopUpBalance() =
    TopUpBalanceResponse(10000f, 1, "BCA", 480,
        "80006289653399715", 0, "2020-05-16 21:29:58")
