package com.dana.batch2.danatopupapp.ui.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.LoginRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.LoginResponse
import com.dana.batch2.danatopupapp.utils.provideLoginResponse
import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class LoginViewModelTest {


    @get:Rule
    val instantTask = InstantTaskExecutorRule()

    @Mock
    private lateinit var mainRepository: MainRepository

    @Mock
    private lateinit var observerLogin: Observer<ApiResponse<LoginResponse>>

    private lateinit var loginViewModel: LoginViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this@LoginViewModelTest)
        loginViewModel = LoginViewModel(mainRepository)
    }

    @Test
    fun postLogin() {
        val loginRequest = LoginRequest("6289653399715", "Qwerty00@@")
        val loginResponse = ApiResponse.success(provideLoginResponse())
        val loginResponseLive = MutableLiveData<ApiResponse<LoginResponse>>()
        loginResponseLive.value = loginResponse

        Mockito.`when`(mainRepository.postLogin(loginRequest)).thenReturn(loginResponseLive)

        loginViewModel.postLogin(loginRequest).observeForever(observerLogin)
        verify(observerLogin).onChanged(loginResponse)
    }
}