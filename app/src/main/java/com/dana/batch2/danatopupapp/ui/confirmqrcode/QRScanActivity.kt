package com.dana.batch2.danatopupapp.ui.confirmqrcode

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.appcompat.app.AppCompatActivity
import com.dana.batch2.danatopupapp.R
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import kotlinx.android.synthetic.main.activity_q_r_scan.*

class QRScanActivity : AppCompatActivity() {

    private lateinit var captureManager: CaptureManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_q_r_scan)

        captureManager = CaptureManager(this, compoundBarcodeView)
        captureManager.initializeFromIntent(intent, savedInstanceState)
        compoundBarcodeView.decodeSingle(object: BarcodeCallback{
            override fun barcodeResult(result: BarcodeResult?) {
                result?.let {
                    //maybe add JSON validation
                    val resultText = it.text
                    doVibrate()
                    val intent = Intent()
                    intent.putExtra("QR Result", resultText)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }

            override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {}
        })

        onClickListener()
        initTorch()
    }

    private fun onClickListener() {
        backFromScanner.setOnClickListener {
            finish()
        }
    }

    private fun initTorch() {
        var torchState = false
        buttonTorch.setOnClickListener {
            if (torchState){
                torchState = false
                compoundBarcodeView.setTorchOff()
                buttonTorch.setIconResource(R.drawable.ic_flash_on_black_24dp)
            }else{
                torchState = true
                compoundBarcodeView.setTorchOn()
                buttonTorch.setIconResource(R.drawable.ic_flash_off_black_24dp)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    @Suppress("DEPRECATION")
    private fun doVibrate(){
        val vib = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if(vib.hasVibrator()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                vib.vibrate(
                    VibrationEffect.createOneShot(
                        100,
                        VibrationEffect.DEFAULT_AMPLITUDE
                    ))
            }else{
                vib.vibrate(100)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        captureManager.onResume()
    }

    override fun onPause() {
        super.onPause()
        captureManager.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        captureManager.onDestroy()
    }
}
