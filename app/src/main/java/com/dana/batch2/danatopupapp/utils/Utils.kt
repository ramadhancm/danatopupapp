package com.dana.batch2.danatopupapp.utils

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Environment
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.view.View
import com.dana.batch2.danatopupapp.data.source.remote.response.PaymentMethodResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpBalanceResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpHistoryResponse
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.journeyapps.barcodescanner.BarcodeEncoder
import okhttp3.ResponseBody
import org.json.JSONObject
import java.io.File
import java.io.IOException
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat


private fun provideLogoPaymentMethod(listMethods: List<PaymentMethodResponse>): List<PaymentMethodResponse> {
    listMethods.forEach {
        when (it.idPayment) {
            1 -> it.logoMerchant =
                "https://3.bp.blogspot.com/-ZK6W9UlA3lw/V15RGexr3yI/AAAAAAAAAJ4/nkyM9ebn_qg3_rQWyBZ1se5L_SSuuxcDACLcB/s1600/Bank_Central_Asia.png"
            2 -> it.logoMerchant =
                "https://seeklogo.com/images/I/indomaret-logo-EE717AAD0D-seeklogo.com.png"
        }
    }

    return listMethods
}

fun processLogoPaymentMethod(paymentMethods: List<PaymentMethodResponse>): MutableList<Any> {
    val dataPayments = mutableListOf<Any>()

    provideLogoPaymentMethod(paymentMethods).forEach {
        if (it.paymentType == 1) {
            if (!dataPayments.contains("Bank")) dataPayments.add("Bank")
            dataPayments.add(it)
        } else {
            if (!dataPayments.contains("Merchant")) dataPayments.add("Merchant")
            dataPayments.add(it)
        }
    }

    return dataPayments
}

fun convertTopUpHistory(data: TopUpHistoryResponse): TopUpBalanceResponse {
    return TopUpBalanceResponse(
        data.topUpBalance,
        data.paymentType,
        data.paymentName,
        data.invoiceId,
        data.paymentToken,
        data.status,
        data.createdAt
    )
}

fun getApiErrorMessage(errorCode: Int, errorBody: ResponseBody?): String{
    return when (errorCode){
        404 -> "Not Found"
        in 500..510 -> "Something went wrong"
        else -> {
            val errorObject = JSONObject(errorBody?.string().toString())
            errorObject.getString("message")
        }
    }
}

fun getPaymentType(type: Int): String {
    val value = "Top Up by "
    return when (type) {
        1 -> "$value Bank"
        else -> "$value Merchant"
    }
}

fun formatSpannableBalance(balance: Int): SpannableStringBuilder {
    val nominal = (balance / 1000).toString()
    val zero = ".000"

    val spanResult = SpannableStringBuilder(nominal)
    spanResult.setSpan(RelativeSizeSpan(2f), 0, nominal.length, 0)
    spanResult.append(zero)
    return spanResult
}


fun formatNominalBalance(balance: Int?): String {

    val idrFormat = DecimalFormat.getCurrencyInstance() as DecimalFormat
    val idrFormatSymbol = DecimalFormatSymbols()

    idrFormatSymbol.currencySymbol = "Rp."
    idrFormatSymbol.groupingSeparator = '.'
    idrFormat.decimalFormatSymbols = idrFormatSymbol
    return if (balance != null) {
        idrFormat.format(balance).dropLast(3)
    } else {
        idrFormat.format(0).dropLast(3)
    }
}

fun formatNominalBalanceNonPrefix(balance: Int?): String {

    val idrFormat = DecimalFormat.getCurrencyInstance() as DecimalFormat
    val idrFormatSymbol = DecimalFormatSymbols()

    idrFormatSymbol.currencySymbol = ""
    idrFormatSymbol.groupingSeparator = '.'
    idrFormat.decimalFormatSymbols = idrFormatSymbol
    return if (balance != null) {
        idrFormat.format(balance).dropLast(3)
    } else {
        idrFormat.format(0).dropLast(3)
    }
}


@SuppressLint("SimpleDateFormat")
fun formatDate(date: String?): String {
    if (date == null || date.isBlank()) return " - "

    val oldFormat = SimpleDateFormat("yyyy-MM-dd")
    val newDate = oldFormat.parse(date)
    val newFormat = SimpleDateFormat("dd-MMM-yyyy")

    return newDate?.let { newFormat.format(it) }.toString()
}

fun convertStatusCode(status: Int): String {
    return when (status) {
        0 -> "Pending"
        1 -> "Success"
        2 -> "Expired"
        else -> "Undefined"
    }
}

fun convertPaymentTypeCode(paymentType: Int): String {
    return when (paymentType) {
        1 -> "Bank Transfer"
        2 -> "Merchant"
        else -> "Undefined"
    }
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}


private const val IMAGE_PREFIX = "Image_"
private const val JPG_SUFFIX = ".png"
private const val FOLDER_NAME = "Photo_session"

@Suppress("DEPRECATION")
fun createDirectoryIfNotExist() {
    val folder = File(
        Environment.getExternalStorageDirectory().toString() +
                File.separator + Environment.DIRECTORY_PICTURES + File.separator + FOLDER_NAME
    )
    if (!folder.exists()) {
        folder.mkdirs()
    }
}

@Suppress("DEPRECATION")
fun createFile() = File(
    Environment.getExternalStorageDirectory().toString() +
            File.separator + Environment.DIRECTORY_PICTURES + File.separator +
            FOLDER_NAME + File.separator + IMAGE_PREFIX + System.currentTimeMillis() + JPG_SUFFIX
)

fun filterValidEmail(email: String): Boolean {
    return when {
        email.takeLast(3).contentEquals(".id") -> true
        email.takeLast(3).contentEquals("com") -> true
        email.takeLast(3).contentEquals(".co") -> true
        else -> false
    }
}

fun filterValidPhoneNumber(phone: String): Boolean {
    return phone.length in 14 downTo 10 && phone.take(2) == "62"
}

fun filterValidateName(name: String): Boolean {
    return name.length in 10 downTo 3
}

@SuppressLint("SimpleDateFormat")
fun calculateExpireTime(timeCreated: String): Long {
    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val result = sdf.parse(timeCreated)
    val fiveMinutesInMillis = 300000L
    val createAt = result?.time ?: 0L
    return (fiveMinutesInMillis + createAt) - System.currentTimeMillis()
}

fun createQrCode(paymentToken: String, invoiceId: Int): Bitmap? {

    val qrCode = "https://336f1b2a.ngrok.io/confirm-merchant-topup/${paymentToken}/${invoiceId}"


    val multiFormat = MultiFormatWriter()
    var qrCodeImage: Bitmap? = null

    try {
        val biMatrix = multiFormat.encode(
            qrCode, BarcodeFormat.QR_CODE, 230, 230
        )

        val barcodeEncoder = BarcodeEncoder()
        qrCodeImage = barcodeEncoder.createBitmap(biMatrix)

    } catch (e: IOException) {
        e.printStackTrace()
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return qrCodeImage
}


