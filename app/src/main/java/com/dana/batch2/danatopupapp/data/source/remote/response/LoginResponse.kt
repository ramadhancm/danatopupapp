package com.dana.batch2.danatopupapp.data.source.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class LoginResponse(
    @SerializedName("token")
    var authToken: String


) : Parcelable