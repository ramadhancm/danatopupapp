package com.dana.batch2.danatopupapp.ui.detailpayment

import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpBalanceResponse

class DetailPaymentViewModel: ViewModel() {

    private var isNewPayment = false
    private var isPaymentHasFinished = false
    private var uploadedPhotoPath = ""

    private lateinit var topUpData: TopUpBalanceResponse


    fun getIsNewPayment(state: Boolean): Boolean {
        isNewPayment = state
        return isNewPayment
    }

    fun getIsNewPayment(): Boolean {
        return isNewPayment
    }

    fun setTopUpData(topUp: TopUpBalanceResponse) {
        topUpData = topUp
    }

    fun getTopUpData(): TopUpBalanceResponse {
        return topUpData
    }

    fun setUploadPhotoPath(photo: String) {
        uploadedPhotoPath = photo
    }

    fun getPhotoPath(): String {
        return uploadedPhotoPath
    }

    fun setIsFinishedTopUp(state: Boolean) {
        isPaymentHasFinished = state
    }

    fun getIsFinishedTopUp(): Boolean {
        return isPaymentHasFinished
    }

    fun setPaymentStatus(state: Int) {
        topUpData.status = state
    }
}