package com.dana.batch2.danatopupapp.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.MainActivity
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.ui.login.LoginActivity

class SplashActivity : AppCompatActivity() {

    companion object {
        private const val SPLASH_TIME_OUT: Long = 2000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val preferences = getSharedPreferences(BuildConfig.PREF, 0)
        val autToken = preferences.getString(BuildConfig.AUTH_TOKEN, "")
        Handler().postDelayed({
            val intent = if (!autToken.isNullOrEmpty()){
                Intent(this, MainActivity::class.java)
            }else{
                Intent(this, LoginActivity::class.java)
            }
            startActivity(intent)
            finish()
        }, SPLASH_TIME_OUT)

    }
}
