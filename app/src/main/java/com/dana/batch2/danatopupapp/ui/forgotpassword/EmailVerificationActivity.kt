package com.dana.batch2.danatopupapp.ui.forgotpassword

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity.Companion.EXTRA_EMAIL
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity.Companion.EXTRA_PHONE
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity.Companion.VERIFY_TYPE
import kotlinx.android.synthetic.main.activity_email_verification.*

class EmailVerificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email_verification)

        setOnClick()
    }

    private fun setOnClick() {
        buttonVerifyEmailForPassword.setOnClickListener {
            doVerifyPassword()
        }
        back_button_forgot_password.setOnClickListener {
            finish()
        }
    }

    private fun processToOTP(email: String) {
        val userPhone = intent?.getStringExtra("PHONE")
        val intent = Intent(this@EmailVerificationActivity, OtpActivity::class.java)
        intent.putExtra(VERIFY_TYPE, false)
        intent.putExtra(EXTRA_PHONE, userPhone)
        intent.putExtra(EXTRA_EMAIL, email)
        startActivity(intent)
        finish()
    }


    private fun doVerifyPassword() {
        inputEmailForgotLayout.error = null
        val email = inputEmailForgotPassword.text.toString()
        if (email.isEmpty()){
            inputEmailForgotLayout.error = "Please fill email field"
        }else{
            processToOTP(email)
        }
    }
}
