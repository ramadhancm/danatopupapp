package com.dana.batch2.danatopupapp.ui.newhome

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserBalanceResponse

class NewHomeViewModel(private val mainRepository: MainRepository) : ViewModel() {
    private var authToken = ""
    fun setAuthToken(token: String) {
        authToken = token
    }

    fun getUserBalance(): LiveData<ApiResponse<UserBalanceResponse>> {
        return mainRepository.getUserBalance(authToken)
    }

}