package com.dana.batch2.danatopupapp.data.source.remote.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.io.File

@Parcelize
data class UploadTransferRequest (
    var transferReceipt: File
) : Parcelable