package com.dana.batch2.danatopupapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.dana.batch2.danatopupapp.databinding.ActivityMainBinding
import com.dana.batch2.danatopupapp.ui.confirmqrcode.QRScanActivity

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        binding.navView.setupWithNavController(navController)

        onClickQrScanner()
    }

    private fun onClickQrScanner() {
        binding.fabScanner.setOnClickListener {
            startActivity(Intent(this@MainActivity, QRScanActivity::class.java))
        }
    }


}
