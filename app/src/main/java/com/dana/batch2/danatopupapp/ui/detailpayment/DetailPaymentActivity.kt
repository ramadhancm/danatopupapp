package com.dana.batch2.danatopupapp.ui.detailpayment

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.CountDownTimer
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dana.batch2.danatopupapp.MainActivity
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.data.source.remote.request.UploadTransferRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpBalanceResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpHistoryResponse
import com.dana.batch2.danatopupapp.databinding.ActivityDetailPaymentBinding
import com.dana.batch2.danatopupapp.ui.confirmqrcode.ConfirmQrCodeActivity
import com.dana.batch2.danatopupapp.ui.confirmqrcode.ConfirmQrCodeActivity.Companion.EXTRA_INVOICE
import com.dana.batch2.danatopupapp.ui.confirmqrcode.ConfirmQrCodeActivity.Companion.EXTRA_PAYMENT_TOKEN
import com.dana.batch2.danatopupapp.ui.confirmreceipt.ConfirmReceiptActivity
import com.dana.batch2.danatopupapp.ui.confirmreceipt.ConfirmReceiptActivity.Companion.EXTRA_RECEIPT
import com.dana.batch2.danatopupapp.ui.confirmreceipt.ConfirmReceiptActivity.Companion.EXTRA_TOKEN_PAYMENT
import com.dana.batch2.danatopupapp.ui.confirmreceipt.ConfirmReceiptActivity.Companion.RESULT_CONFIRM_PHOTO
import com.dana.batch2.danatopupapp.ui.uploadreceipt.ImagePopupView
import com.dana.batch2.danatopupapp.utils.*

class DetailPaymentActivity : AppCompatActivity() {

    private lateinit var paymentViewModel: DetailPaymentViewModel
    private lateinit var binding: ActivityDetailPaymentBinding

    private var imagePopupView: ImagePopupView? = null
    private var timer: CountDownTimer? = null

    companion object {
        private const val PENDING_CODE = 0
        private const val SUCCESS_CODE = 1
        private const val BANK_PAYMENT = 1
        private const val QR_CODE_PAYMENT = 2
        private const val REQUEST_CONFIRM_PAYMENT = 101
        const val EXTRA_PAYMENT = "extra_payment"
        const val IS_NEW_PAYMENT = "extra_new"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailPaymentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
        setOnClickListener()

    }

    override fun onResume() {
        super.onResume()
        if (paymentViewModel.getTopUpData().status == PENDING_CODE) {
            setExpireTime()
        } else {
            if (paymentViewModel.getTopUpData().status == SUCCESS_CODE) {
                updateSuccessUi()
            } else {
                updateExpireUi()
            }
        }
    }

    private fun init() {
        initViewModel()
        setIsNewPayment()
    }

    private fun setOnClickListener() {
        onClickBackArrow()
        onClickEnlargerImage()
    }

    private fun initViewModel() {
        paymentViewModel =
            ViewModelProvider(
                this@DetailPaymentActivity,
                ViewModelProvider.NewInstanceFactory()
            ).get(
                DetailPaymentViewModel::class.java
            )
    }

    private fun setIsNewPayment() {
        val state = intent?.getBooleanExtra(IS_NEW_PAYMENT, false) ?: false
        if (paymentViewModel.getIsNewPayment(state)) {
            loadDetailNewPayment()
        } else {
            loadDetailOldPayment()
        }
    }

    private fun loadDetailNewPayment() {
        val topUpPayment = intent.getParcelableExtra<TopUpBalanceResponse>(EXTRA_PAYMENT)
        topUpPayment?.let { nonNullPaymentData ->
            binding.apply {
                tvBalance.text = formatNominalBalance(nonNullPaymentData.topUpBalance.toInt())
                tvStatus.text = convertStatusCode(nonNullPaymentData.status)
                tvDate.text = formatDate(nonNullPaymentData.createdAt)
                tvPaymentType.text = convertPaymentTypeCode(nonNullPaymentData.paymentType)
                tvPaymentMethod.text = nonNullPaymentData.paymentName
                tvVirtualAccount.text = nonNullPaymentData.paymentToken
                tvInvoice.text = nonNullPaymentData.invoiceId.toString()

                paymentViewModel.setTopUpData(nonNullPaymentData)
                onClickConfirm()
            }
        }
    }

    private fun loadDetailOldPayment() {
        val topUpHistory = intent.getParcelableExtra<TopUpHistoryResponse>(EXTRA_PAYMENT)
        topUpHistory?.let { nonNullPaymentData ->
            binding.apply {
                tvBalance.text = formatNominalBalance(nonNullPaymentData.topUpBalance.toInt())
                tvStatus.text = convertStatusCode(nonNullPaymentData.status)
                tvDate.text = formatDate(nonNullPaymentData.createdAt)
                tvPaymentType.text = convertPaymentTypeCode(nonNullPaymentData.paymentType)
                tvPaymentMethod.text = nonNullPaymentData.paymentName
                tvVirtualAccount.text = nonNullPaymentData.paymentToken
                tvInvoice.text = nonNullPaymentData.invoiceId.toString()
                paymentViewModel.setUploadPhotoPath(nonNullPaymentData.filePath ?: "")
                paymentViewModel.setTopUpData(convertTopUpHistory(nonNullPaymentData))
                onClickConfirm()
            }
        }
    }

    private fun setExpireTime() {
        val expireTime = calculateExpireTime(paymentViewModel.getTopUpData().createdAt)
        if (expireTime < 0) {
            updateExpireUi()
        } else {
            countDownExpire(expireTime)
        }
    }

    private fun countDownExpire(expireTime: Long) {

        timer = object : CountDownTimer(expireTime, 1000) {
            override fun onFinish() {
                updateExpireUi()
            }

            override fun onTick(p0: Long) {
                updateTimer(p0)
            }
        }

        timer?.start()
    }

    private fun updateTimer(expireTimes: Long) {
        val second = (expireTimes / 1000) % 60
        val minutes = expireTimes / (1000 * 60) % 60
        val countDown = "$minutes:$second"
        binding.tvTimer.text = countDown
    }

    private fun updateExpireUi() {
        binding.tvStatus.text = getString(R.string.expired)
        binding.tvTimer.text = getString(R.string.timeout)
        expireConfirmButton()
    }

    private fun updateSuccessUi() {
        timer?.cancel()
        binding.tvStatus.text = getString(R.string.success)
        binding.tvTimer.text = "-"
        finishedConfirmButton()
        updatePaymentProf()
    }

    private fun updatePaymentProf() {
        if (paymentViewModel.getTopUpData().paymentType == BANK_PAYMENT
            && paymentViewModel.getTopUpData().status == PENDING_CODE
        ) {

            Glide.with(this@DetailPaymentActivity)
                .load(paymentViewModel.getPhotoPath())
                .apply(RequestOptions().error(R.drawable.ic_photo_camera_black_24dp))
                .into(binding.imgProfPayment)

        } else if (paymentViewModel.getTopUpData().paymentType == QR_CODE_PAYMENT) {

            Glide.with(this@DetailPaymentActivity)
                .load(
                    createQrCode(
                        paymentViewModel.getTopUpData().paymentToken,
                        paymentViewModel.getTopUpData().invoiceId
                    )
                )
                .apply(RequestOptions().error(R.drawable.ic_photo_camera_black_24dp))
                .into(binding.imgProfPayment)
        }
    }

    private fun onClickConfirm() {
        binding.btnConfirm.setOnClickListener {
            when (paymentViewModel.getTopUpData().paymentType) {
                BANK_PAYMENT -> {
                    val uploadIntent =
                        Intent(this@DetailPaymentActivity, ConfirmReceiptActivity::class.java)
                    uploadIntent.putExtra(
                        EXTRA_TOKEN_PAYMENT,
                        paymentViewModel.getTopUpData().paymentToken
                    )
                    startActivityForResult(uploadIntent, REQUEST_CONFIRM_PAYMENT)
                }

                QR_CODE_PAYMENT -> {
                    val qrIntent = Intent(
                        this@DetailPaymentActivity,
                        ConfirmQrCodeActivity::class.java
                    )
                    qrIntent.putExtra(EXTRA_INVOICE, paymentViewModel.getTopUpData().invoiceId)
                    qrIntent.putExtra(
                        EXTRA_PAYMENT_TOKEN,
                        paymentViewModel.getTopUpData().paymentToken
                    )

                    startActivity(qrIntent)
                }
            }
        }
    }

    private fun finishedConfirmButton() {
        binding.btnConfirm.text = getString(R.string.payment_finish)
        binding.btnConfirm.isEnabled = false
    }

    private fun expireConfirmButton() {
        binding.btnConfirm.text = getString(R.string.payment_has_expire)
        binding.btnConfirm.isEnabled = false
    }

    private fun onClickBackArrow() {
        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        if (paymentViewModel.getIsNewPayment()) {
            onBackAlertDialog()
        } else {
            if (binding.rootView.isVisible) {
                removeImagePopup()
            } else {
                super.onBackPressed()
            }
        }
    }

    private fun onClickEnlargerImage() {
        binding.imgProfPayment.setOnClickListener {
            showImagePopup()
        }
    }

    private fun showImagePopup() {
        if (binding.imgProfPayment.drawable == null) return

        binding.rootView.visible()
        createImagePopup(binding.imgProfPayment.drawable) { removeImagePopup() }
            .let {
                imagePopupView = it
                addImagePopupViewToRoot(it)
            }
    }

    private fun createImagePopup(
        imageDrawable: Drawable,
        backgroundClickAction: () -> Unit
    ) =
        ImagePopupView.builder(this)
            .imageDrawable(imageDrawable)
            .onBackgroundClickAction(backgroundClickAction)
            .build()

    private fun removeImagePopup() {
        binding.rootView.invisible()
        imagePopupView?.let {
            it.animate()
                .alpha(ImagePopupView.ALPHA_TRANSPARENT)
                .setDuration(ImagePopupView.FADING_ANIMATION_DURATION)
                .withEndAction {
                    binding.rootView.removeView(it)
                }
                .start()
        }
    }

    private fun addImagePopupViewToRoot(imagePopupView: ImagePopupView) {
        binding.rootView.addView(
            imagePopupView,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
    }

    private fun onBackAlertDialog() {
        val toHomeIntent = Intent(this@DetailPaymentActivity, MainActivity::class.java)
        toHomeIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK

        if (!paymentViewModel.getIsFinishedTopUp()) {
            AlertDialog.Builder(this@DetailPaymentActivity).apply {
                setTitle(getString(R.string.sure_want_leave))
                setMessage(getString(R.string.transaction_unfinish))
                setPositiveButton(getString(R.string.yes)) { _, _ ->
                    startActivity(toHomeIntent)
                }
                setNegativeButton(getString(R.string.no)) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                setCancelable(false)
            }.create().show()
        } else {
            startActivity(toHomeIntent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CONFIRM_PAYMENT && resultCode == RESULT_CONFIRM_PHOTO) {
            val receiptProf = data?.getParcelableExtra<UploadTransferRequest>(EXTRA_RECEIPT)
            Glide.with(this@DetailPaymentActivity).load(receiptProf?.transferReceipt)
                .into(binding.imgProfPayment)
            paymentViewModel.setPaymentStatus(SUCCESS_CODE)
            paymentViewModel.setIsFinishedTopUp(true)
            updateSuccessUi()

        }
    }
}
