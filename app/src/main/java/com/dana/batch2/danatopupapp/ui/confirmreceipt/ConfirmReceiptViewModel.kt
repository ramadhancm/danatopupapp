package com.dana.batch2.danatopupapp.ui.confirmreceipt

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.UploadTransferRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.NonBodyResponse

class ConfirmReceiptViewModel(private val mainRepository: MainRepository): ViewModel() {

    private var authToken = ""

    fun setAuthToken(token: String) {
        authToken = token
    }

    private var paymentToken: String = ""

    fun setPaymentToken(token: String) {
        paymentToken = token
    }

    fun processUploadReceipt(uploadTransfer: UploadTransferRequest): LiveData<ApiResponse<NonBodyResponse>> {
        return mainRepository.postTransferReceipt(authToken, uploadTransfer, paymentToken)
    }

}