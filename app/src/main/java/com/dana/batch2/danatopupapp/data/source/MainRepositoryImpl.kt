package com.dana.batch2.danatopupapp.data.source
import androidx.lifecycle.LiveData
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.RemoteDataSource
import com.dana.batch2.danatopupapp.data.source.remote.request.*
import com.dana.batch2.danatopupapp.data.source.remote.response.*
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val remoteData: RemoteDataSource
) : MainRepository {
    override fun postLogin(loginRequest: LoginRequest): LiveData<ApiResponse<LoginResponse>> {
        return remoteData.postLogin(loginRequest)
    }

    override fun postSignUp(signUpRequest: SignUpRequest): LiveData<ApiResponse<NonBodyResponse>> {
        return remoteData.postSignUp(signUpRequest)
    }

    override fun postSendOtp(
        sendOtpRequest: SendOtpRequest
    ): LiveData<ApiResponse<NonBodyResponse>> {
        return remoteData.postSendOtp(sendOtpRequest)
    }

    override fun postVerifyOtp(
        verifyOtpRequest: VerifyOtpRequest
    ): LiveData<ApiResponse<NonBodyResponse>> {
        return remoteData.postVerifyOtp(verifyOtpRequest)
    }

    override fun getUserProfile(authToken: String): LiveData<ApiResponse<UserProfileResponse>> {
        return remoteData.getUserProfile(authToken)
    }

    override fun getUserBalance(authToken: String): LiveData<ApiResponse<UserBalanceResponse>> {
        return remoteData.getUserBalance(authToken)
    }

    override fun getCatalogBalance(authToken: String): LiveData<ApiResponse<List<BalanceCatalogResponse>>> {
        return remoteData.getCatalogBalance(authToken)
    }

    override fun getPaymentMethod(authToken: String): LiveData<ApiResponse<List<PaymentMethodResponse>>> {
        return remoteData.getPaymentMethod(authToken)
    }

    override fun getTopUpHistory(authToken: String): LiveData<ApiResponse<List<TopUpHistoryResponse>>> {
        return remoteData.getTopUpHistory(authToken)
    }

    override fun postTopUpBalance(
        authToken: String,
        topUpBalanceRequest: TopUpBalanceRequest
    ): LiveData<ApiResponse<TopUpBalanceResponse>> {
        return remoteData.postTopUpBalance(authToken, topUpBalanceRequest)
    }

    override fun postTransferReceipt(
        authToken: String,
        uploadTransferRequest: UploadTransferRequest,
        paymentToken: String
    ): LiveData<ApiResponse<NonBodyResponse>> {
        return remoteData.postTransferReceipt(authToken, uploadTransferRequest, paymentToken)
    }
}