package com.dana.batch2.danatopupapp.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.MainActivity
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.LoginRequest
import com.dana.batch2.danatopupapp.ui.forgotpassword.EmailVerificationActivity
import com.dana.batch2.danatopupapp.ui.signup.SignUpActivity
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.utils.visible
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initInject()
        initViewModel()
        onClickGroup()
        inputUsername.setPrefix("62")
    }

    private fun initInject() {
        (application as DanaTopUpApplication).dataTopUpApplication.inject(this)
    }

    private fun initViewModel() {
        loginViewModel =
            ViewModelProvider(this@LoginActivity, viewModelFactory).get(LoginViewModel::class.java)
    }

    private fun onClickGroup() {
        btnLogin.setOnClickListener {
            if (inputUsername.text?.isNotEmpty()!! && inputPassword.text.isNotEmpty()) {
                loginProgressBar.visible()
                processLogin(inputUsername.text.toString(), inputPassword.text.toString())
            } else {
                Toast.makeText(this, getString(R.string.user_pass_empty), Toast.LENGTH_SHORT).show()
            }
        }

        textRegister.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        textForgotPassword.setOnClickListener {
            Toast.makeText(this, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun processLogin(phoneNumber: String, password: String) {
        loginViewModel.postLogin(LoginRequest(phoneNumber, password))
            .observe(this, Observer { apiResponse ->
                loginProgressBar.invisible()
                when (apiResponse.status) {
                    StatusResponse.SUCCESS -> {
                        apiResponse.body?.authToken?.let { nonNullToken ->
                            saveTokenToPreference(nonNullToken, phoneNumber)
                        }

                        val loginIntent = Intent(this@LoginActivity, MainActivity::class.java)
                        loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(loginIntent)
                    }
                    StatusResponse.EMPTY -> {
                        toVerification(apiResponse.message)
                        Toast.makeText(this, "${apiResponse.message}", Toast.LENGTH_SHORT)
                            .show()

                    }
                    StatusResponse.ERROR -> {
                        Toast.makeText(this, getString(R.string.failed_login), Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            })
    }

    private fun toVerification(message: String?) {
        if (message != null) {
            if(message.contains(getString(R.string.please_active))){
                val intent = Intent(this@LoginActivity, EmailVerificationActivity::class.java)
                intent.putExtra(BuildConfig.AUTH_PHONE, inputUsername.text.toString())
                startActivity(intent)
            }
        }
    }

    private fun saveTokenToPreference(authToken: String, phoneNumber: String) {
        val preference = getSharedPreferences(BuildConfig.PREF, 0)
        val editor = preference.edit()
        editor.putString(BuildConfig.AUTH_TOKEN, "Bearer $authToken")
        editor.putString(BuildConfig.AUTH_PHONE, phoneNumber)
        editor.apply()
    }
}



