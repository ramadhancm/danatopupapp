package com.dana.batch2.danatopupapp.ui.confirmqrcode

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserProfileResponse

class ConfirmQrCodeViewModel(private val mainRepository: MainRepository) : ViewModel() {

    private var authToken = ""

    fun setAuthToken(token: String) {
        authToken = token
    }

    fun getUserProfile(): LiveData<ApiResponse<UserProfileResponse>> {
        return mainRepository.getUserProfile(authToken)
    }
}