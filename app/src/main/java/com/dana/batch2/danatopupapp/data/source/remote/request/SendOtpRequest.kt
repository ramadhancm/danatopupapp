package com.dana.batch2.danatopupapp.data.source.remote.request

import com.google.gson.annotations.SerializedName


data class SendOtpRequest(
    @SerializedName("phone_number")
    var phoneNumber: String,

    @SerializedName("email")
    var email: String
)