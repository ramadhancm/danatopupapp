package com.dana.batch2.danatopupapp.ui.paymenttype

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.data.source.remote.request.TopUpBalanceRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.BalanceCatalogResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.PaymentMethodResponse
import com.dana.batch2.danatopupapp.utils.convertPaymentTypeCode
import com.dana.batch2.danatopupapp.utils.formatNominalBalance
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_payment_modal.*


class PaymentTypeModalFragment : BottomSheetDialogFragment(), View.OnClickListener {

    companion object {
        private const val EXTRA_PAYMENT = "extra_payment"
        private const val EXTRA_BALANCE = "extra_balance"
        private const val EXTRA_PHONE = "extra_phone"
        private var listener: ItemClickListener? = null
        const val TAG = "bottom_dialog"

        fun newInstance(
            phoneNumber: String,
            balance: BalanceCatalogResponse?,
            payment: PaymentMethodResponse,
            itemClickListener: ItemClickListener
        ): PaymentTypeModalFragment {
            val fragment = PaymentTypeModalFragment()
            val bundle = Bundle().apply {
                putString(EXTRA_PHONE, phoneNumber )
                putParcelable(EXTRA_BALANCE, balance)
                putParcelable(EXTRA_PAYMENT, payment)
            }

            listener = itemClickListener
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_modal, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val phoneNumber = arguments?.getString(EXTRA_PHONE) ?: ""
        val balance = arguments?.getParcelable<BalanceCatalogResponse>(EXTRA_BALANCE)
        val payment = arguments?.getParcelable<PaymentMethodResponse>(EXTRA_PAYMENT)

        onClickCloseIcon()

        balance?.let { nonNullBalance ->
            payment?.let { nonNullPayment ->
                loadPurchaseDetail(nonNullBalance, nonNullPayment)
                onClickPurchase(TopUpBalanceRequest(phoneNumber, nonNullBalance.code, nonNullPayment.paymentType))
            }
        }
    }

    private fun loadPurchaseDetail(
        balanceCatalog: BalanceCatalogResponse,
        paymentMethod: PaymentMethodResponse
    ) {
        tv_balance.text = formatNominalBalance(balanceCatalog.balance)
        tv_payment_type.text = convertPaymentTypeCode(paymentMethod.paymentType)
        tv_payment.text = paymentMethod.paymentName
        Glide.with(requireActivity()).load(paymentMethod.logoMerchant).into(img_merchant)

    }

    override fun onClick(p0: View?) {
        dismiss()
    }

    private fun onClickPurchase(topUpBalanceRequest: TopUpBalanceRequest) {
        btn_purchase.setOnClickListener {
            listener?.onItemClick(topUpBalanceRequest)
            dismiss()
        }
    }

    private fun onClickCloseIcon() {
        img_close.setOnClickListener { dismiss() }
    }

    interface ItemClickListener {
        fun onItemClick(topUpBalanceRequest: TopUpBalanceRequest)
    }
}
