package com.dana.batch2.danatopupapp.ui.confirmreceipt

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse.*
import com.dana.batch2.danatopupapp.data.source.remote.request.UploadTransferRequest
import com.dana.batch2.danatopupapp.databinding.ActivityConfirmReceiptBinding
import com.dana.batch2.danatopupapp.ui.confirmreceipt.ConfirmReceiptDialogFragment.ItemDialogClick
import com.dana.batch2.danatopupapp.ui.uploadreceipt.UploadReceiptActivity
import com.dana.batch2.danatopupapp.ui.uploadreceipt.UploadReceiptActivity.Companion.EXTRA_PHOTO
import com.dana.batch2.danatopupapp.ui.uploadreceipt.UploadReceiptActivity.Companion.RESULT_TAKE_PHOTO
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.utils.visible
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import java.io.File
import javax.inject.Inject

class ConfirmReceiptActivity : AppCompatActivity(), ItemDialogClick {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var confirmReceiptViewModel: ConfirmReceiptViewModel
    private lateinit var binding: ActivityConfirmReceiptBinding

    companion object {
        const val RESULT_CONFIRM_PHOTO = 102
        const val REQUEST_TAKE_PHOTO = 201
        const val REQUEST_GALLERY_PHOTO = 203
        const val EXTRA_RECEIPT = "extra_receipt"
        const val EXTRA_TOKEN_PAYMENT = "extra_payment"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmReceiptBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
        setOnClickListener()
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        setAuthToken()
        setPaymentToken()

        Glide.with(this@ConfirmReceiptActivity).load(R.drawable.ic_photo_camera_black_24dp)
            .apply(RequestOptions().override(180, 240)).into(binding.imgReceipt)
    }

    fun setOnClickListener() {
        onClickTakePhoto()
        onClickPhoto()
        onClickBackArrow()
    }

    private fun injectViewModelFactory() {
        (application as DanaTopUpApplication).dataTopUpApplication.inject(this@ConfirmReceiptActivity)
    }

    private fun initViewModel() {
        confirmReceiptViewModel =
            ViewModelProvider(this@ConfirmReceiptActivity, viewModelFactory).get(
                ConfirmReceiptViewModel::class.java
            )
    }

    private fun setAuthToken() {
        val preference = getSharedPreferences(BuildConfig.PREF, 0)
        val authTokenPref = preference.getString(BuildConfig.AUTH_TOKEN, "") ?: ""
        confirmReceiptViewModel.setAuthToken(authTokenPref)
    }

    private fun setPaymentToken() {
        val paymentToken = intent?.getStringExtra(EXTRA_TOKEN_PAYMENT) ?: ""
        confirmReceiptViewModel.setPaymentToken(paymentToken)
    }

    private fun onClickTakePhoto() {
        binding.tvTakePhoto.setOnClickListener {
            val intent = Intent(this@ConfirmReceiptActivity, UploadReceiptActivity::class.java)
            startActivityForResult(intent, REQUEST_TAKE_PHOTO)
        }
    }

    private fun onClickPhoto() {
        binding.imgReceipt.setOnClickListener {
            val ft = supportFragmentManager.beginTransaction()
            val dialogFragment = ConfirmReceiptDialogFragment.newInstance(this)
            ft.addToBackStack(null)
            dialogFragment.show(ft, "DIALOG")

        }
    }

    private fun onClickConfirm(uploadTransfer: UploadTransferRequest) {
        binding.btnConfirm.isEnabled = true
        binding.btnConfirm.setOnClickListener {
            showConfirmDialog(uploadTransfer)
        }
    }

    private fun onClickBackArrow() {
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    private fun processUploadReceipt(uploadTransfer: UploadTransferRequest) {
        Log.d("state", "uploadTransfer: $uploadTransfer")
        confirmReceiptViewModel.processUploadReceipt(uploadTransfer)
            .observe(this@ConfirmReceiptActivity, Observer { apiResponse ->
                when (apiResponse.status) {
                    SUCCESS -> {
                        binding.progressBar.invisible()
                        val uploadIntent = Intent()
                        uploadIntent.putExtra(EXTRA_RECEIPT, uploadTransfer)
                        setResult(RESULT_CONFIRM_PHOTO, uploadIntent)
                        finish()
                    }

                    EMPTY -> {
                        binding.progressBar.invisible()
                        Toast.makeText(
                            this@ConfirmReceiptActivity,
                            apiResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    ERROR -> {
                        binding.progressBar.invisible()
                        Toast.makeText(
                            this@ConfirmReceiptActivity,
                            getString(R.string.failed_upload),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            })
    }

    private fun showImageFromCameraResult(data: Intent?) {
        val fileReceipt = data?.getParcelableExtra<UploadTransferRequest>(EXTRA_PHOTO)
        fileReceipt?.let { nonNullUploadReceipt ->
            Glide.with(this@ConfirmReceiptActivity).load(nonNullUploadReceipt.transferReceipt)
                .apply(RequestOptions().override(180, 240)).into(binding.imgReceipt)
            onClickConfirm(nonNullUploadReceipt)
        }
    }

    private fun showImageFromGalleryResult(data: Intent?) {
        val selectedUri = data?.data?.let { getRealPathFromURI(it) }
        val imageFile = File(selectedUri.toString())
        val uploadRequest = UploadTransferRequest(imageFile)
        Glide.with(this@ConfirmReceiptActivity).load(uploadRequest.transferReceipt)
            .apply(RequestOptions().override(180, 240)).into(binding.imgReceipt)

        onClickConfirm(uploadRequest)
    }

    private fun showConfirmDialog(uploadTransfer: UploadTransferRequest) {
        AlertDialog.Builder(this@ConfirmReceiptActivity).apply {
            setTitle(getString(R.string.sure_want_to_choose))
            setMessage(getString(R.string.cannot_replace))
            setPositiveButton(getString(R.string.confirm)){ _, _ ->
                processUploadReceipt(uploadTransfer)
                binding.progressBar.visible()
            }
            setNegativeButton(getString(R.string.cancel)){ dialogInterface, _ ->
                dialogInterface.cancel()
            }.create().show()
        }
    }

    @Suppress("DEPRECATION")
    private fun getRealPathFromURI(contentUri: Uri): String {
        val result: String
        val cursor = contentResolver.
        query(contentUri, null, null,null, null)

        if (cursor == null) {
            result = contentUri.path.toString()
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }

        return result
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_TAKE_PHOTO) {
            showImageFromCameraResult(data)
        }

        if (requestCode == REQUEST_GALLERY_PHOTO) {
            showImageFromGalleryResult(data)
        }
    }

    override fun onClickCamera() {
        val intent = Intent(this@ConfirmReceiptActivity, UploadReceiptActivity::class.java)
        startActivityForResult(intent, REQUEST_TAKE_PHOTO)
    }

    override fun onClickGallery() {
        val galleryIntent = Intent()
        galleryIntent.type = getString(R.string.source_image)
        galleryIntent.action = Intent.ACTION_PICK //ACTION_GET_CONTENT Crashes in some devices
        startActivityForResult(
            Intent.createChooser(galleryIntent, getString(R.string.select_picture)),
            REQUEST_GALLERY_PHOTO
        )
    }
}
