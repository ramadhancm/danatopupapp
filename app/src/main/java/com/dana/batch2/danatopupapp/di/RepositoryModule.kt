package com.dana.batch2.danatopupapp.di

import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.MainRepositoryImpl
import com.dana.batch2.danatopupapp.data.source.remote.RemoteDataSource
import com.dana.batch2.danatopupapp.data.source.remote.RemoteDataSourceImpl
import com.dana.batch2.danatopupapp.rest.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {


    @Provides
    @Singleton
    fun provideRemoteDataSource(apiService: ApiService): RemoteDataSource {
        return RemoteDataSourceImpl(apiService)
    }

    @Provides
    @Singleton
    fun provideMainDataSource(remoteData: RemoteDataSourceImpl): MainRepository {
        return MainRepositoryImpl(remoteData)
    }
}