package com.dana.batch2.danatopupapp.data.source.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentMethodResponse(

    @SerializedName("id")
    var idPayment: Int,

    @SerializedName("payment_type")
    var paymentType: Int,

    @SerializedName("name")
    var paymentName: String,

    var logoMerchant: String = ""
) : Parcelable