package com.dana.batch2.danatopupapp.data.source.remote.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BalanceCatalogResponse(

    @SerializedName("id")
    var id: Int,

    @SerializedName("code")
    var code: String,

    @SerializedName("balance")
    var balance: Int
) : Parcelable