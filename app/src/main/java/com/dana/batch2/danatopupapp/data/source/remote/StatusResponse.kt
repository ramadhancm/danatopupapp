package com.dana.batch2.danatopupapp.data.source.remote

enum class StatusResponse {
    SUCCESS,
    EMPTY,
    ERROR
}
