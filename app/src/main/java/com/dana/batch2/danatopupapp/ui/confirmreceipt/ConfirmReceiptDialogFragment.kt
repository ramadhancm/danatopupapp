package com.dana.batch2.danatopupapp.ui.confirmreceipt

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.dana.batch2.danatopupapp.databinding.FragmentConfirmReceiptDialogBinding


class ConfirmReceiptDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentConfirmReceiptDialogBinding

    companion object {
        private var dialogListener: ItemDialogClick? = null

        fun newInstance(
            dialogClick: ItemDialogClick
        ): ConfirmReceiptDialogFragment {
            dialogListener = dialogClick
            return ConfirmReceiptDialogFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentConfirmReceiptDialogBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onClickCamera()
        onClickGallery()
    }

    private fun onClickCamera() {
        binding.tvCamera.setOnClickListener {
            dialogListener?.onClickCamera()
            dismiss()
        }
    }

    private fun onClickGallery() {
        binding.tvGallery.setOnClickListener {
            dialogListener?.onClickGallery()
            dismiss()
        }
    }

    interface ItemDialogClick {
        fun onClickCamera()
        fun onClickGallery()
    }
}
