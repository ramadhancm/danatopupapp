package com.dana.batch2.danatopupapp.ui.otp

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.SendOtpRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.VerifyOtpRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.NonBodyResponse

class OtpViewModel(private val mainRepository: MainRepository): ViewModel() {

    fun postSendOtp(sendOtpRequest: SendOtpRequest): LiveData<ApiResponse<NonBodyResponse>> {
        return mainRepository.postSendOtp(sendOtpRequest)
    }

    fun postVerifyOtp(verifyOtpRequest: VerifyOtpRequest): LiveData<ApiResponse<NonBodyResponse>> {
        return mainRepository.postVerifyOtp(verifyOtpRequest)
    }

}