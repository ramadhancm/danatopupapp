package com.dana.batch2.danatopupapp.ui.paymenttype

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.data.source.remote.response.PaymentMethodResponse
import com.dana.batch2.danatopupapp.utils.processLogoPaymentMethod

class PaymentTypeAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val dataPayments = mutableListOf<Any>()

    private val paymentMethods = mutableListOf<PaymentMethodResponse>()

    private lateinit var listener: (PaymentMethodResponse) -> Unit

    fun setPaymentMethodData(list: List<PaymentMethodResponse>) {
        paymentMethods.clear()
        paymentMethods.addAll(list)
        setPaymentMethodLogo()
        notifyDataSetChanged()
    }

    private fun setPaymentMethodLogo() {
        dataPayments.clear()
        dataPayments.addAll(processLogoPaymentMethod(paymentMethods))
    }

    fun setOnClickPaymentMethod(listener: (PaymentMethodResponse) -> Unit) {
        this.listener = listener
    }

    companion object {
        private const val ITEM_HEADER = 0
        private const val ITEM_MENU = 1
    }

    override fun getItemViewType(position: Int): Int {
        return when (dataPayments[position]) {
            is String -> ITEM_HEADER
            is PaymentMethodResponse -> ITEM_MENU
            else -> throw IllegalArgumentException("Undefined view type")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_header_payment, parent, false)
        return when (viewType) {
            ITEM_HEADER -> PaymentTypeHeaderHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_header_payment, parent, false)
            )

            ITEM_MENU -> PaymentTypeMenuHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_menu_payment, parent, false)
            )

            else -> throw throw IllegalArgumentException("Undefined view type")
        }
    }

    override fun getItemCount(): Int {
        return dataPayments.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            ITEM_HEADER -> {
                val headerHolder = holder as PaymentTypeHeaderHolder
                headerHolder.bindPaymentHeader(dataPayments[position] as String)
            }

            ITEM_MENU -> {
                val menuHolder = holder as PaymentTypeMenuHolder
                menuHolder.bindPaymentMenu(dataPayments[position] as PaymentMethodResponse, listener)
            }

            else -> throw throw IllegalArgumentException("Undefined view type")
        }
    }
}