package com.dana.batch2.danatopupapp.data.source

import androidx.lifecycle.LiveData
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.*
import com.dana.batch2.danatopupapp.data.source.remote.response.*

interface MainRepository {
    fun postLogin(loginRequest: LoginRequest):
            LiveData<ApiResponse<LoginResponse>>

    fun postSignUp(signUpRequest: SignUpRequest):
            LiveData<ApiResponse<NonBodyResponse>>

    fun postSendOtp(sendOtpRequest: SendOtpRequest):
            LiveData<ApiResponse<NonBodyResponse>>

    fun postVerifyOtp(verifyOtpRequest: VerifyOtpRequest):
            LiveData<ApiResponse<NonBodyResponse>>

    fun getUserProfile(authToken: String):
            LiveData<ApiResponse<UserProfileResponse>>

    fun getUserBalance(authToken: String):
            LiveData<ApiResponse<UserBalanceResponse>>

    fun getCatalogBalance(authToken: String):
            LiveData<ApiResponse<List<BalanceCatalogResponse>>>

    fun getPaymentMethod(authToken: String):
            LiveData<ApiResponse<List<PaymentMethodResponse>>>

    fun getTopUpHistory(authToken: String):
            LiveData<ApiResponse<List<TopUpHistoryResponse>>>

    fun postTopUpBalance(authToken: String, topUpBalanceRequest: TopUpBalanceRequest):
            LiveData<ApiResponse<TopUpBalanceResponse>>

    fun postTransferReceipt(authToken: String, uploadTransferRequest: UploadTransferRequest, paymentToken: String):
            LiveData<ApiResponse<NonBodyResponse>>

}