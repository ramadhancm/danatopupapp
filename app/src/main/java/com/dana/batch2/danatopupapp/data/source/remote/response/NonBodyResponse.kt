package com.dana.batch2.danatopupapp.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class NonBodyResponse(
    @SerializedName("message")
    var message: String,
    @SerializedName("status")
    var status: Int
)