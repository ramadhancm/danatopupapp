package com.dana.batch2.danatopupapp.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.LoginRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.LoginResponse

class LoginViewModel(private val mainRepository: MainRepository): ViewModel() {

    fun postLogin(loginRequest: LoginRequest): LiveData<ApiResponse<LoginResponse>> {
        return mainRepository.postLogin(loginRequest)
    }
}