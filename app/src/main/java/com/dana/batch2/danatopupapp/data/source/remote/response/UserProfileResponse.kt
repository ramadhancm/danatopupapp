package com.dana.batch2.danatopupapp.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class UserProfileResponse (

    @SerializedName("email")
    var email: String,

    @SerializedName("first_name")
    var firstName: String,

    @SerializedName("last_name")
    var lastName: String,

    @SerializedName("phone_number")
    var phoneNumber: String



)