package com.dana.batch2.danatopupapp.ui.otp

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse.*
import com.dana.batch2.danatopupapp.data.source.remote.request.SendOtpRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.SignUpRequest
import com.dana.batch2.danatopupapp.data.source.remote.request.VerifyOtpRequest
import com.dana.batch2.danatopupapp.databinding.ActivityOtpBinding
import com.dana.batch2.danatopupapp.ui.login.LoginActivity
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import javax.inject.Inject

class OtpActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var otpViewModel: OtpViewModel
    private lateinit var binding: ActivityOtpBinding
    private lateinit var timer: CountDownTimer

    companion object {
        const val EXTRA_USER_PROFILE = "extra_user_profile"
        const val EXTRA_PHONE = "user_phone"
        const val EXTRA_EMAIL = "user_email"
        const val VERIFY_TYPE = "sign_up"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        getUserProfile()
        onClickBackNavigation()
    }


    private fun injectViewModelFactory() {
        (application as DanaTopUpApplication).dataTopUpApplication.inject(this@OtpActivity)
    }

    private fun initViewModel() {
        otpViewModel =
            ViewModelProvider(this@OtpActivity, viewModelFactory).get(OtpViewModel::class.java)
    }

    private fun getUserProfile() {
        val verifyType = intent?.getBooleanExtra(VERIFY_TYPE, false) ?: false

        if (verifyType) {
            val userProfile = intent?.getParcelableExtra<SignUpRequest>(EXTRA_USER_PROFILE)
            userProfile?.let { nonNullProfile ->
                processOtp(nonNullProfile.email, nonNullProfile.phoneNumber)
            }
        } else {
            val phoneNumber = intent?.getStringExtra(EXTRA_PHONE).toString()
            val email = intent?.getStringExtra(EXTRA_EMAIL).toString()
            sendOtpRequest(email, phoneNumber)
            processOtp(email, phoneNumber)
        }
    }

    private fun processOtp(email: String, phoneNumber: String) {
        val emailAndPhone = "$phoneNumber or $email"
        binding.tvEmailPhone.text = emailAndPhone
        countDown(email, phoneNumber)
        setOtpListener(phoneNumber)
    }


    private fun countDown(email: String, phoneNumber: String) {
        timer = object : CountDownTimer(300000, 1000) {
            override fun onFinish() {
                onClickResendCode(email, phoneNumber)
            }

            override fun onTick(p0: Long) {
                updateUi(p0)
            }
        }
        timer.start()
    }

    private fun updateUi(times: Long) {
        val second = (times / 1000) % 60
        val minute = times / (1000 * 60) % 60
        val countDown = "$minute:$second"
        binding.tvTimer.text = countDown
    }

    private fun sendOtpRequest(email: String, phoneNumber: String) {

        otpViewModel.postSendOtp(SendOtpRequest(phoneNumber, email))
            .observe(this@OtpActivity, Observer { apiResponse ->
                when (apiResponse.status) {
                    SUCCESS -> {
                        Toast.makeText(
                            this@OtpActivity,
                            getString(R.string.success_request_otp),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    EMPTY -> {
                        timer.onFinish()
                        Toast.makeText(
                            this@OtpActivity,
                            apiResponse.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        if (apiResponse.message!!.contains("wrong")) {
                            finish()
                        }
                    }

                    ERROR -> {
                        timer.onFinish()
                        Toast.makeText(
                            this@OtpActivity,
                            getString(R.string.failed_request_otp),
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                }
            })
    }

    private fun verifyOtp(otpCode: String, phoneNumber: String) {

        otpViewModel.postVerifyOtp(VerifyOtpRequest(phoneNumber, otpCode.toInt()))
            .observe(this@OtpActivity, Observer { apiResponse ->
                when (apiResponse.status) {
                    SUCCESS -> {
                        Toast.makeText(
                            this@OtpActivity,
                            getString(R.string.success_verify_otp),
                            Toast.LENGTH_SHORT
                        ).show()

                        val toLoginIntent = Intent(this@OtpActivity, LoginActivity::class.java)
                        toLoginIntent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(toLoginIntent) // dummy
                    }

                    EMPTY -> {
                        showErrorMessage()
                    }

                    ERROR -> {
                        showErrorMessage()
                    }
                }
            })
    }

    private fun showErrorMessage() {
        Toast.makeText(
            this@OtpActivity,
            getString(R.string.failed_verify_otp),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun setOtpListener(phoneNumber: String) {
        binding.otpView.setOtpCompletionListener { otpCode ->
            onClickValidate(otpCode, phoneNumber)
        }
    }

    private fun onClickResendCode(email: String, phoneNumber: String) {
        val resend = "Resend Code"
        binding.tvValidate.text = resend
        binding.tvValidate.setOnClickListener {
            binding.tvValidate.text = getString(R.string.validate)
            sendOtpRequest(email, phoneNumber)
            timer.start()

            Toast.makeText(this@OtpActivity, "Send your otp code", Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickValidate(otpCode: String, phoneNumber: String) {
        binding.tvValidate.setOnClickListener {
            verifyOtp(otpCode, phoneNumber)
        }
    }

    private fun onClickBackNavigation() {
        binding.imgBack.setOnClickListener {
            finish()
        }
    }
}
