package com.dana.batch2.danatopupapp.ui.history

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpHistoryResponse
import com.dana.batch2.danatopupapp.ui.detailpayment.DetailPaymentActivity
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.utils.visible
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_history_finished.*
import javax.inject.Inject


class FinishedHistoryFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var historyAdapter: HistoryAdapter
    private lateinit var historyViewModel: HistoryViewModel

    companion object {
        private const val PENDING_CODE = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history_finished, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        refreshInProgress.isRefreshing = true
        init()
    }

    override fun onResume() {
        super.onResume()
        getTopUpHistory()
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        setAuthToken()
        initAdapter()
        onClickHistory()
        onRefresh()
    }

    private fun injectViewModelFactory() {
        // Init dependency injection
        (requireActivity().application as DanaTopUpApplication)
            .dataTopUpApplication.inject(this@FinishedHistoryFragment)
    }

    private fun initViewModel() {
        // init ViewModel
        historyViewModel =
            ViewModelProvider(this, viewModelFactory).get(HistoryViewModel::class.java)
    }

    private fun setAuthToken() {
        val preference = requireActivity().getSharedPreferences(BuildConfig.PREF, 0)
        val authTokenPref = preference.getString(BuildConfig.AUTH_TOKEN, "") ?: ""
        historyViewModel.setAuthToken(authTokenPref)
    }

    private fun initAdapter() {
        historyAdapter = HistoryAdapter()
        rvFinishedHistoryLayout.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = historyAdapter
        }
    }

    private fun loadTopUpBalance(balanceCatalogs: List<TopUpHistoryResponse>) {
        historyAdapter.setHistory(balanceCatalogs.filter { it.status != PENDING_CODE }.reversed())

    }

    private fun getTopUpHistory() {
        historyViewModel.getTopUpHistory().observe(viewLifecycleOwner, Observer { apiResponse ->
            when (apiResponse.status) {
                StatusResponse.SUCCESS -> {
                    apiResponse.body?.let { balanceCatalogs ->
                        loadTopUpBalance(balanceCatalogs)
                        showEmptyData(balanceCatalogs)
                    }

                    refreshInProgress.isRefreshing = false
                }
                StatusResponse.EMPTY -> {
                    refreshInProgress.isRefreshing = false
                    if (apiResponse.message.toString().equals("not found", true)){
                        tvEmptyTopUp.visible()
                    } else {
                        showErrorMessage()
                    }
                }
                StatusResponse.ERROR -> {
                    refreshInProgress.isRefreshing = false
                    showErrorMessage()
                }
            }
        })
    }

    private fun onClickHistory() {
        historyAdapter.setOnClickHistory { historyTopUp ->
            processToDetailPayment(historyTopUp)
        }
    }

    private fun processToDetailPayment(historyResponse: TopUpHistoryResponse) {
        val transactionIntent = Intent(context, DetailPaymentActivity::class.java)
        transactionIntent.putExtra(DetailPaymentActivity.IS_NEW_PAYMENT, false)
        transactionIntent.putExtra(DetailPaymentActivity.EXTRA_PAYMENT, historyResponse)
        startActivity(transactionIntent)
    }

    private fun showEmptyData(topUpHistory: List<TopUpHistoryResponse>) {
        if (historyViewModel.isFinishedTopUpEmpty(topUpHistory, PENDING_CODE)) {
            tvEmptyTopUp.visible()
        } else {
            tvEmptyTopUp.invisible()
        }
    }

    private fun onRefresh() {
        refreshInProgress.setOnRefreshListener {
            getTopUpHistory()
        }
    }

    private fun showErrorMessage(message: String = getString(R.string.bad_internet)) {
        Toast.makeText(
            context, message,
            Toast.LENGTH_SHORT
        ).show()
    }
}
