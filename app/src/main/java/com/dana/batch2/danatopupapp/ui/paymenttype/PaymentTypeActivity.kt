package com.dana.batch2.danatopupapp.ui.paymenttype

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.TopUpBalanceRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.BalanceCatalogResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.PaymentMethodResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpBalanceResponse
import com.dana.batch2.danatopupapp.databinding.ActivityPaymentTypeBinding
import com.dana.batch2.danatopupapp.ui.detailpayment.DetailPaymentActivity
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.utils.visible
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import javax.inject.Inject

class PaymentTypeActivity : AppCompatActivity(), PaymentTypeModalFragment.ItemClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var paymentTypeViewModel: PaymentTypeViewModel
    private lateinit var paymentAdapter: PaymentTypeAdapter

    private lateinit var preference: SharedPreferences
    private lateinit var authTokenPref: String
    private lateinit var binding: ActivityPaymentTypeBinding

    companion object {
        const val EXTRA_BALANCE = "extra_balance"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentTypeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

        val phoneNumber = preference.getString(BuildConfig.AUTH_PHONE, "") ?: ""
        val balanceCatalog =
            intent?.getParcelableExtra<BalanceCatalogResponse>(EXTRA_BALANCE)

        setOnClickListener(balanceCatalog, phoneNumber)
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        initSharedPreference()
        initAdapter()
        initRecyclerView()
        setAuthToken()
        getPaymentMethod()
    }

    private fun injectViewModelFactory() {
        (application as DanaTopUpApplication)
            .dataTopUpApplication.inject(this@PaymentTypeActivity)
    }

    private fun initViewModel() {
        paymentTypeViewModel =
            ViewModelProvider(
                this@PaymentTypeActivity,
                viewModelFactory
            ).get(PaymentTypeViewModel::class.java)
    }

    private fun initSharedPreference() {
        preference = getSharedPreferences(BuildConfig.PREF, 0)
        authTokenPref = preference.getString(BuildConfig.AUTH_TOKEN, "") ?: ""
    }

    private fun initAdapter() {
        paymentAdapter = PaymentTypeAdapter()
    }

    private fun initRecyclerView() {
        binding.rvPaymentType.apply {
            layoutManager = LinearLayoutManager(this@PaymentTypeActivity)
            setHasFixedSize(true)
            adapter = paymentAdapter
        }
    }

    private fun setAuthToken() {
        paymentTypeViewModel.setAuthToken(authTokenPref)
    }

    private fun setOnClickListener(
        balanceCatalog: BalanceCatalogResponse?,
        phoneNumber: String
    ) {
        onClickPaymentMethod(balanceCatalog, phoneNumber)
        onClickBackArrow()
    }

    private fun loadPaymentMethod(paymentMethods: List<PaymentMethodResponse>) {
        paymentAdapter.setPaymentMethodData(paymentMethods)
    }

    private fun getPaymentMethod() {
        binding.progressBar.visible()
        paymentTypeViewModel.getPaymentMethod()
            .observe(this@PaymentTypeActivity, Observer { apiResponse ->
                when (apiResponse.status) {
                    StatusResponse.SUCCESS -> {
                        binding.progressBar.invisible()
                        apiResponse.body?.let { paymentMethods ->
                            loadPaymentMethod(paymentMethods)
                        }
                    }

                    StatusResponse.EMPTY -> {
                        binding.progressBar.invisible()
                        showErrorMessage()
                    }

                    StatusResponse.ERROR -> {
                        binding.progressBar.invisible()
                        showErrorMessage()
                    }
                }
            })
    }

    private fun postTopUpBalance(topUpBalanceRequest: TopUpBalanceRequest) {
        paymentTypeViewModel.postTopUpBalance(topUpBalanceRequest)
            .observe(this@PaymentTypeActivity, Observer { apiResponse ->
                when (apiResponse.status) {
                    StatusResponse.SUCCESS -> {
                        apiResponse.body?.let { topUpResponse ->
                            processToDetailPayment(topUpResponse)
                            binding.progressBar.invisible()
                        }
                    }

                    StatusResponse.EMPTY -> {
                        binding.progressBar.invisible()
                        showErrorMessage()
                    }

                    StatusResponse.ERROR -> {
                        binding.progressBar.invisible()
                        showErrorMessage()
                    }
                }
            })
    }

    private fun processToDetailPayment(topUpResponse: TopUpBalanceResponse) {
        val transactionIntent =
            Intent(this@PaymentTypeActivity, DetailPaymentActivity::class.java)
        transactionIntent.putExtra(DetailPaymentActivity.IS_NEW_PAYMENT, true)
        transactionIntent.putExtra(DetailPaymentActivity.EXTRA_PAYMENT, topUpResponse)
        startActivity(transactionIntent)
    }

    private fun onClickPaymentMethod(
        balanceCatalog: BalanceCatalogResponse?,
        phoneNumber: String
    ) {
        paymentAdapter.setOnClickPaymentMethod { paymentType ->
            val paymentModalFragment =
                PaymentTypeModalFragment.newInstance(phoneNumber, balanceCatalog, paymentType, this)
            paymentModalFragment.show(supportFragmentManager, PaymentTypeModalFragment.TAG)
        }
    }

    private fun onClickBackArrow() {
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    override fun onItemClick(topUpBalanceRequest: TopUpBalanceRequest) {
        postTopUpBalance(topUpBalanceRequest)
        binding.progressBar.visible()
    }

    private fun showErrorMessage() {
        Toast.makeText(
            this@PaymentTypeActivity, getString(R.string.bad_internet),
            Toast.LENGTH_SHORT
        ).show()
    }
}
