package com.dana.batch2.danatopupapp.ui.confirmqrcode

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse.*
import com.dana.batch2.danatopupapp.data.source.remote.response.UserProfileResponse
import com.dana.batch2.danatopupapp.databinding.ActivityConfirmQrCodeBinding
import com.dana.batch2.danatopupapp.utils.createQrCode
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import javax.inject.Inject

class ConfirmQrCodeActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var confirmQrCodeViewModel: ConfirmQrCodeViewModel
    private lateinit var binding: ActivityConfirmQrCodeBinding

    companion object {
        const val EXTRA_INVOICE = "extra_invoice"
        const val EXTRA_PAYMENT_TOKEN = "extra_token"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmQrCodeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        setAuthToken()
        getUserProfile()

        val paymentToken = intent?.getStringExtra(EXTRA_PAYMENT_TOKEN) ?: ""
        val invoiceId = intent.getIntExtra(EXTRA_INVOICE, 0)
        loadQrCode(paymentToken, invoiceId)
        onClickBackArrow()
    }

    private fun injectViewModelFactory() {
        (application as DanaTopUpApplication).dataTopUpApplication.inject(this@ConfirmQrCodeActivity)
    }

    private fun initViewModel() {
        confirmQrCodeViewModel =
            ViewModelProvider(this@ConfirmQrCodeActivity, viewModelFactory).get(
                ConfirmQrCodeViewModel::class.java
            )
    }

    private fun setAuthToken() {
        val preference = getSharedPreferences(BuildConfig.PREF, 0)
        val authTokenPref = preference.getString(BuildConfig.AUTH_TOKEN, "") ?: ""
        confirmQrCodeViewModel.setAuthToken(authTokenPref)
    }

    private fun getUserProfile() {
        confirmQrCodeViewModel.getUserProfile()
            .observe(this@ConfirmQrCodeActivity, Observer { apiResponse ->
                when (apiResponse.status) {
                    SUCCESS -> {
                        loadNameUser(apiResponse.body)
                    }

                    EMPTY -> {
                    }

                    ERROR -> {
                    }
                }
            })
    }

    private fun loadNameUser(userData: UserProfileResponse?) {
        userData?.let { nonNullUserData ->
            val fullName = "${nonNullUserData.firstName} ${nonNullUserData.lastName}"
            binding.tvNameUser.text = fullName
        }
    }

    private fun loadQrCode(paymentToken: String, invoiceId: Int) {
        Glide.with(this@ConfirmQrCodeActivity).load(createQrCode(paymentToken, invoiceId))
            .into(binding.imgQrCode)

    }

    private fun onClickBackArrow() {
        binding.imgBack.setOnClickListener { finish() }
    }

}
