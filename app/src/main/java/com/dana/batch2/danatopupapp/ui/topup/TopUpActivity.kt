package com.dana.batch2.danatopupapp.ui.topup

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse.*
import com.dana.batch2.danatopupapp.data.source.remote.response.BalanceCatalogResponse
import com.dana.batch2.danatopupapp.databinding.ActivityTopUpBinding
import com.dana.batch2.danatopupapp.ui.paymenttype.PaymentTypeActivity
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.utils.visible
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import javax.inject.Inject

class TopUpActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var topUpViewModel: TopUpViewModel
    private lateinit var topUpAdapter: TopUpAdapter
    private lateinit var binding: ActivityTopUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTopUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.progressBar.visible()
        init()
        setOnClickListener()
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        initAdapter()
        initRecyclerview()
        initSwipeLayout()
        setAuthToken()
        getBalanceCatalog()
    }

    private fun injectViewModelFactory() {
        // Init dependency injection
        (application as DanaTopUpApplication)
            .dataTopUpApplication.inject(this@TopUpActivity)
    }

    private fun initViewModel() {
        // init ViewModel
        topUpViewModel =
            ViewModelProvider(this@TopUpActivity, viewModelFactory).get(TopUpViewModel::class.java)
    }


    private fun initAdapter() {
        topUpAdapter = TopUpAdapter()
    }

    private fun initRecyclerview() {
        binding.rvBalanceCatalog.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = topUpAdapter
        }
    }

    private fun initSwipeLayout() {
        binding.swipeLayout.setOnRefreshListener {
            showProgressBar()
            getBalanceCatalog()
        }
    }

    private fun setAuthToken() {
        val preference = getSharedPreferences(BuildConfig.PREF, 0)
        val authTokenPref = preference.getString(BuildConfig.AUTH_TOKEN, "") ?: ""
        topUpViewModel.setAuthToken(authTokenPref)
    }

    private fun loadBalanceCatalog(balanceCatalogs: List<BalanceCatalogResponse>) {
        topUpAdapter.setBalanceCatalog(balanceCatalogs)
    }

    private fun getBalanceCatalog() {

        topUpViewModel.getBalanceCatalog().observe(this@TopUpActivity, Observer { apiResponse ->
            when (apiResponse.status) {
                SUCCESS -> {
                    apiResponse.body?.let { balanceCatalogs ->
                        loadBalanceCatalog(balanceCatalogs)
                    }
                    hideProgressBar()
                }
                EMPTY -> {
                    showErrorMessage()
                    hideProgressBar()
                }
                ERROR -> {
                    showErrorMessage()
                    hideProgressBar()
                }
            }
        })
    }

    private fun setOnClickListener() {
        onClickBalanceCatalog()
        onClickBackArrow()
    }

    private fun onClickBalanceCatalog() {
        topUpAdapter.setOnClickCatalog { balance, isChecked ->
            onClickPaymentMethod(balance, isChecked)
        }
    }

    private fun onClickPaymentMethod(balance: BalanceCatalogResponse, isChecked: Boolean) {
        binding.btnSelectPayment.isEnabled = isChecked
        binding.btnSelectPayment.setOnClickListener {

            val intent = Intent(this@TopUpActivity, PaymentTypeActivity::class.java)
            intent.putExtra(PaymentTypeActivity.EXTRA_BALANCE, balance)
            startActivity(intent)
        }
    }

    private fun onClickBackArrow() {
        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun showProgressBar() {
        binding.swipeLayout.isRefreshing = true
    }

    private fun hideProgressBar() {
        binding.progressBar.invisible()
        binding.swipeLayout.isRefreshing = false
    }

    private fun showErrorMessage() {
        Toast.makeText(
            this@TopUpActivity, getString(R.string.bad_internet),
            Toast.LENGTH_SHORT
        ).show()
    }
}
