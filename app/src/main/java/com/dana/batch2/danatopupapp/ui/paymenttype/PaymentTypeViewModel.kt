package com.dana.batch2.danatopupapp.ui.paymenttype

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.TopUpBalanceRequest
import com.dana.batch2.danatopupapp.data.source.remote.response.PaymentMethodResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpBalanceResponse

class PaymentTypeViewModel(private val mainRepository: MainRepository): ViewModel() {

    private var authToken = ""

    fun setAuthToken(token: String) {
        authToken = token
    }

    fun getPaymentMethod(): LiveData<ApiResponse<List<PaymentMethodResponse>>> {
        return mainRepository.getPaymentMethod(authToken)
    }

    fun postTopUpBalance(topUpBalance: TopUpBalanceRequest): LiveData<ApiResponse<TopUpBalanceResponse>> {
        return mainRepository.postTopUpBalance(authToken, topUpBalance)
    }

}