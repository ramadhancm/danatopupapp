package com.dana.batch2.danatopupapp.ui.signup

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse
import com.dana.batch2.danatopupapp.data.source.remote.request.SignUpRequest
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity.Companion.EXTRA_USER_PROFILE
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity.Companion.VERIFY_TYPE
import com.dana.batch2.danatopupapp.utils.*
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_sign_up.*
import javax.inject.Inject

class SignUpActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var signUpViewModel: SignUpViewModel

    companion object {
        private val PHONE_PATTERN = "^[0-9+]*$".toRegex()
        private val EMAIL_PATTERN = "^[\\w-.]+@([\\w-]+\\.)+[\\w-]{2,4}\$".toRegex()
        private val PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{6,}".toRegex()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        (application as DanaTopUpApplication).dataTopUpApplication.inject(this)
        signUpViewModel = ViewModelProvider(
            this@SignUpActivity,
            viewModelFactory
        ).get(SignUpViewModel::class.java)
        onClickListener()
    }

    private fun onClickListener() {
        back_button_sign_up.setOnClickListener {
            finish()
        }
        buttonRegister.setOnClickListener {
            emptyError()
            val firstName = inputRegisterFirstName.text.toString()
            val lastName = inputRegisterLastName.text.toString()
            val email = inputRegisterEmail.text.toString()
            val phoneNumber = inputRegisterPhone.text.toString()
            val password = inputRegisterPassword.text.toString()
            val passwordConfirm = inputRegisterPasswordVerify.text.toString()
            var errorState = false

            if (firstName.isEmpty()) {
                registerFirstNameLayout.error = "First Name field is required!"
                errorState = true
            } else if (!filterValidateName(firstName)) {
                registerFirstNameLayout.error = "Minimal character is 3 and maximal character is 10"
                errorState = true
            }

            if (lastName.isEmpty()) {
                registerLastNameLayout.error = "Last Name field is required"
                errorState = true
            } else if (!filterValidateName(lastName)) {
                registerLastNameLayout.error = "Minimal character is 3 and maximal character is 10"
                errorState = true
            }

            if (email.isEmpty()) {
                registerEmailLayout.error = "Email field is required"
                errorState = true
            } else if (!EMAIL_PATTERN.matches(email)) {
                registerEmailLayout.error = "Not an Email pattern"
                errorState = true
            } else if (!filterValidEmail(email)) {
                registerEmailLayout.error = "Please input valid TLD (co, com, id)"
                errorState = true
            }



            if (phoneNumber.isEmpty()) {
                registerPhoneLayout.error = "Phone Number field is required"
                errorState = true
            } else if (!PHONE_PATTERN.matches(phoneNumber)) {
                registerPhoneLayout.error = "Phone Number is not valid"
                errorState = true
            } else if (!filterValidPhoneNumber(phoneNumber)) {
                registerPhoneLayout.error = "Please input valid number 6289xxx and only consist 10 - 14 digit"
                errorState = true
            }

            if (password.isEmpty()) {
                registerPasswordLayout.error = "Password field is required"
                errorState = true
            } else if (!PASSWORD_PATTERN.matches(password)) {
                Log.d("state", "Password: $password and ${PASSWORD_PATTERN.matches(password)}")
                registerPasswordLayout.error = "Didn't match password pattern requirement"
                errorState = true
            }

            if (password != passwordConfirm || passwordConfirm.isEmpty()) {
                registerPasswordVerifyLayout.error = "Password not verified"
                errorState = true
            }

            if (errorState) return@setOnClickListener

            val signUpRequest = SignUpRequest(email, firstName, lastName, password, phoneNumber)
            progressBarRegister.visible()
            requestRegister(signUpRequest)
        }
    }

    private fun emptyError() {
        registerFirstNameLayout.error = null
        registerLastNameLayout.error = null
        registerEmailLayout.error = null
        registerPhoneLayout.error = null
        registerPasswordLayout.error = null
        registerPasswordVerifyLayout.error = null
    }

    private fun processToOtpActivity(signUpRequest: SignUpRequest) {
        val signUpIntent = Intent(this@SignUpActivity, OtpActivity::class.java)
        signUpIntent.putExtra(VERIFY_TYPE, true)
        signUpIntent.putExtra(EXTRA_USER_PROFILE, signUpRequest)
        startActivity(signUpIntent)
    }

    private fun requestRegister(signUpRequest: SignUpRequest) {
        signUpViewModel.processSignUp(signUpRequest).observe(this, Observer { response ->

            when (response.status) {
                StatusResponse.SUCCESS -> {
                    processToOtpActivity(signUpRequest)
                }

                StatusResponse.ERROR -> {
                    Toast.makeText(
                        this,
                        getString(R.string.failed_sign_up),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                StatusResponse.EMPTY -> {
                    Toast.makeText(
                        this,
                        response.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            progressBarRegister.invisible()
        })
    }
}
