package com.dana.batch2.danatopupapp.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class BaseResponse<T>(
    @SerializedName("status")
    var status: Int,

    @SerializedName("data")
    var data: T,

    @SerializedName("message")
    var message: String

    )