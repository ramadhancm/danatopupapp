package com.dana.batch2.danatopupapp.ui.paymenttype

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_header_payment.view.*

class PaymentTypeHeaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindPaymentHeader(paymentHeader: String) {
        itemView.tv_header_payment.text = paymentHeader
    }
}