package com.dana.batch2.danatopupapp.ui.topup

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dana.batch2.danatopupapp.data.source.remote.response.BalanceCatalogResponse
import com.dana.batch2.danatopupapp.databinding.ItemBalanceCatalogBinding
import com.dana.batch2.danatopupapp.ui.topup.TopUpAdapter.HomeViewHolder
import com.dana.batch2.danatopupapp.utils.formatSpannableBalance
import kotlinx.android.synthetic.main.item_balance_catalog.view.*

class TopUpAdapter : RecyclerView.Adapter<HomeViewHolder>() {

    private val balanceCatalogs = mutableListOf<BalanceCatalogResponse>()
    private var lastCheckedPosition = RecyclerView.NO_POSITION
    private lateinit var listener: ((BalanceCatalogResponse, Boolean) -> Unit)

    fun setOnClickCatalog(listener: (BalanceCatalogResponse, Boolean) -> Unit) {
        this.listener = listener
    }

    fun setBalanceCatalog(listBalance: List<BalanceCatalogResponse>) {
        balanceCatalogs.clear()
        balanceCatalogs.addAll(listBalance)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val binding =
            ItemBalanceCatalogBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return HomeViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return balanceCatalogs.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.bindData(balanceCatalogs[position], listener)
        holder.itemView.card_catalog.isChecked = lastCheckedPosition == position
    }

    inner class HomeViewHolder(private val binding: ItemBalanceCatalogBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindData(
            balance: BalanceCatalogResponse,
            listener: (BalanceCatalogResponse, Boolean) -> Unit
        ) {
            binding.tvBalanceCatalog.text = formatSpannableBalance(balance.balance)

            binding.cardCatalog.setOnClickListener {
                if (adapterPosition == RecyclerView.NO_POSITION) {
                    return@setOnClickListener
                }

                if (binding.cardCatalog.isChecked) {
                    binding.cardCatalog.isChecked = false
                    listener(balance, false)
                    return@setOnClickListener
                }

                notifyItemChanged(lastCheckedPosition)
                lastCheckedPosition = adapterPosition
                notifyItemChanged(lastCheckedPosition)
                listener(balance, true)
            }
        }
    }
}