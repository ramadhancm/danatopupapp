package com.dana.batch2.danatopupapp.di

import com.dana.batch2.danatopupapp.rest.ApiClient
import com.dana.batch2.danatopupapp.rest.ApiService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideApiService(): ApiService {
        return ApiClient().getClient()
    }
}