package com.dana.batch2.danatopupapp.ui.paymenttype

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dana.batch2.danatopupapp.data.source.remote.response.PaymentMethodResponse
import kotlinx.android.synthetic.main.item_menu_payment.view.*

class PaymentTypeMenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bindPaymentMenu(
        paymentMethod: PaymentMethodResponse,
        listener: (PaymentMethodResponse) -> Unit
    ) {
        itemView.tv_menu_payment.text = paymentMethod.paymentName
        Glide.with(itemView.context).load(paymentMethod.logoMerchant).into(itemView.img_merchant)
        itemView.setOnClickListener { listener(paymentMethod) }
    }
}