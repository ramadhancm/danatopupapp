package com.dana.batch2.danatopupapp.ui.newhome

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserBalanceResponse
import com.dana.batch2.danatopupapp.databinding.FragmentNewHomeBinding
import com.dana.batch2.danatopupapp.utils.formatNominalBalanceNonPrefix
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import javax.inject.Inject


class NewHomeFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var newHomeViewModel: NewHomeViewModel
    private lateinit var binding: FragmentNewHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentNewHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.progressBar.visibility = View.VISIBLE
        init()
        setOnClickComingSoon()
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        setAuthToken()
        initSwipeLayout()
        getUserBalance()
        onClickTopUp()
    }

    private fun injectViewModelFactory() {
        // Init dependency injection
        (requireActivity().application as DanaTopUpApplication)
            .dataTopUpApplication.inject(this@NewHomeFragment)
    }

    private fun initViewModel() {
        // init ViewModel
        newHomeViewModel =
            ViewModelProvider(
                this@NewHomeFragment,
                viewModelFactory
            ).get(NewHomeViewModel::class.java)
    }

    private fun setOnClickComingSoon() {
        onClickSend()
        onClickReceive()
        onClickMobileCharge()
        onClickAssurance()
        onClickGame()
        onClickWater()
        onClickInternet()
        onClickPln()
        onClickTvCable()
        onClickMore()
    }


    private fun setAuthToken() {
        val preference = requireActivity().getSharedPreferences(BuildConfig.PREF, 0)
        val authTokenPref = preference.getString(BuildConfig.AUTH_TOKEN, "") ?: ""
        newHomeViewModel.setAuthToken(authTokenPref)
    }

    private fun loadUserBalance(userBalance: UserBalanceResponse?) {
        binding.tvBalance.text = formatNominalBalanceNonPrefix(userBalance?.amount?.toInt())
    }

    private fun getUserBalance() {
        newHomeViewModel.getUserBalance().observe(viewLifecycleOwner, Observer { apiResponse ->
            when (apiResponse.status) {
                StatusResponse.SUCCESS -> {
                    loadUserBalance(apiResponse?.body)
                    hideProgressBar()
                }

                StatusResponse.EMPTY -> {
                    showErrorMessage()
                    hideProgressBar()
                }

                StatusResponse.ERROR -> {
                    showErrorMessage()
                    hideProgressBar()
                }
            }
        })
    }

    private fun initSwipeLayout() {
        binding.swipeLayout.setOnRefreshListener {
            getUserBalance()
        }
    }

    private fun showErrorMessage() {
        Toast.makeText(context, getString(R.string.bad_internet),
            Toast.LENGTH_SHORT).show()
    }

    private fun hideProgressBar() {
        binding.progressBar.invisible()
        binding.swipeLayout.isRefreshing = false
    }

    private fun onClickTopUp() {
        binding.tvTopUp.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_navigation_home_to_topUpActivity)
        }
    }

    private fun onClickSend(){
        binding.tvSendMoney.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickReceive() {
        binding.tvReceiveMoney.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickMobileCharge() {
        binding.cardIcon.tvMobile.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickAssurance() {
        binding.cardIcon.tvAssurance.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickGame() {
        binding.cardIcon.tvGame.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickWater() {
        binding.cardIcon.tvWater.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickInternet() {
        binding.cardIcon.tvInternet.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickPln() {
        binding.cardIcon.tvVolt.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickTvCable() {
        binding.cardIcon.tvTelevision.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }

    private fun onClickMore() {
        binding.cardIcon.tvMore.setOnClickListener {
            Toast.makeText(context, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show()
        }
    }
}
