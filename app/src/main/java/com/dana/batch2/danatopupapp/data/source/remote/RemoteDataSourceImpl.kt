package com.dana.batch2.danatopupapp.data.source.remote

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dana.batch2.danatopupapp.data.source.remote.request.*
import com.dana.batch2.danatopupapp.data.source.remote.response.*
import com.dana.batch2.danatopupapp.rest.ApiService
import com.dana.batch2.danatopupapp.utils.getApiErrorMessage
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(
    private val apiService: ApiService
) : RemoteDataSource {

    override fun postLogin(loginRequest: LoginRequest): LiveData<ApiResponse<LoginResponse>> {
        val loginLiveData = MutableLiveData<ApiResponse<LoginResponse>>()

        apiService.postLogin(loginRequest).enqueue(object : Callback<BaseResponse<LoginResponse>?> {
            override fun onFailure(call: Call<BaseResponse<LoginResponse>?>, t: Throwable) {
                loginLiveData.postValue(ApiResponse.error(t.message, null))
            }

            override fun onResponse(
                call: Call<BaseResponse<LoginResponse>?>,
                response: Response<BaseResponse<LoginResponse>?>
            ) {
                if (response.isSuccessful) {
                    loginLiveData.postValue(ApiResponse.success(response.body()?.data))
                } else {
                    loginLiveData.postValue(
                        ApiResponse.empty(
                            getApiErrorMessage(response.code(), response.errorBody()),
                            response.body()?.data
                        )
                    )
                    response.errorBody()!!.close()
                }
            }
        })

        return loginLiveData
    }

    override fun postSignUp(signUpRequest: SignUpRequest): LiveData<ApiResponse<NonBodyResponse>> {
        val signUpLiveData = MutableLiveData<ApiResponse<NonBodyResponse>>()

        apiService.postSignUp(signUpRequest).enqueue(object : Callback<NonBodyResponse?> {
            override fun onFailure(call: Call<NonBodyResponse?>, t: Throwable) {
                signUpLiveData.postValue(ApiResponse.error(t.message, null))
            }

            override fun onResponse(
                call: Call<NonBodyResponse?>,
                response: Response<NonBodyResponse?>
            ) {
                if (response.isSuccessful) {
                    signUpLiveData.postValue(ApiResponse.success(response.body()))
                } else {
                    signUpLiveData.postValue(
                        ApiResponse.empty(
                            getApiErrorMessage(response.code(), response.errorBody()),
                            response.body()
                        )
                    )
                    response.errorBody()!!.close()
                }
            }
        })

        return signUpLiveData
    }


    override fun postSendOtp(sendOtpRequest: SendOtpRequest): LiveData<ApiResponse<NonBodyResponse>> {
        val sendOtpLiveData = MutableLiveData<ApiResponse<NonBodyResponse>>()

        apiService.postSendOtp(sendOtpRequest)
            .enqueue(object : Callback<NonBodyResponse?> {
                override fun onFailure(call: Call<NonBodyResponse?>, t: Throwable) {
                    sendOtpLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<NonBodyResponse?>,
                    response: Response<NonBodyResponse?>
                ) {
                    if (response.isSuccessful) {
                        sendOtpLiveData.postValue(ApiResponse.success(response.body()))
                    } else {
                        sendOtpLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()
                            )
                        )
                    }
                }
            })

        return sendOtpLiveData
    }

    override fun postVerifyOtp(verifyOtpRequest: VerifyOtpRequest): LiveData<ApiResponse<NonBodyResponse>> {
        val verifyOtpLiveData = MutableLiveData<ApiResponse<NonBodyResponse>>()

        apiService.postVerifyOtp(verifyOtpRequest)
            .enqueue(object : Callback<NonBodyResponse?> {
                override fun onFailure(call: Call<NonBodyResponse?>, t: Throwable) {
                    verifyOtpLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<NonBodyResponse?>,
                    response: Response<NonBodyResponse?>
                ) {
                    if (response.isSuccessful) {
                        verifyOtpLiveData.postValue(ApiResponse.success(response.body()))
                    } else {
                        verifyOtpLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()
                            )
                        )
                    }
                }
            })

        return verifyOtpLiveData
    }

    override fun getUserProfile(authToken: String): LiveData<ApiResponse<UserProfileResponse>> {
        val userProfileLiveData = MutableLiveData<ApiResponse<UserProfileResponse>>()

        apiService.getUserProfile(authToken)
            .enqueue(object : Callback<BaseResponse<UserProfileResponse>?> {
                override fun onFailure(
                    call: Call<BaseResponse<UserProfileResponse>?>,
                    t: Throwable
                ) {
                    userProfileLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<BaseResponse<UserProfileResponse>?>,
                    response: Response<BaseResponse<UserProfileResponse>?>
                ) {
                    if (response.isSuccessful) {
                        userProfileLiveData.postValue(ApiResponse.success(response.body()?.data))
                    } else {
                        userProfileLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()?.data
                            )
                        )
                    }
                }
            })

        return userProfileLiveData
    }

    override fun getUserBalance(authToken: String): LiveData<ApiResponse<UserBalanceResponse>> {
        val userBalanceLiveData = MutableLiveData<ApiResponse<UserBalanceResponse>>()

        apiService.getUserBalance(authToken)
            .enqueue(object : Callback<UserBalanceResponse?> {
                override fun onFailure(call: Call<UserBalanceResponse?>, t: Throwable) {
                    userBalanceLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<UserBalanceResponse?>,
                    response: Response<UserBalanceResponse?>
                ) {
                    if (response.isSuccessful) {
                        userBalanceLiveData.postValue(ApiResponse.success(response.body()))
                    } else {
                        userBalanceLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()
                            )
                        )
                    }
                }
            })

        return userBalanceLiveData
    }

    override fun getCatalogBalance(authToken: String): LiveData<ApiResponse<List<BalanceCatalogResponse>>> {
        val balanceCatalogLiveData = MutableLiveData<ApiResponse<List<BalanceCatalogResponse>>>()

        apiService.getBalanceCatalog(authToken)
            .enqueue(object : Callback<BaseResponse<List<BalanceCatalogResponse>>?> {
                override fun onFailure(
                    call: Call<BaseResponse<List<BalanceCatalogResponse>>?>,
                    t: Throwable
                ) {
                    balanceCatalogLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<BaseResponse<List<BalanceCatalogResponse>>?>,
                    response: Response<BaseResponse<List<BalanceCatalogResponse>>?>
                ) {
                    if (response.isSuccessful) {
                        balanceCatalogLiveData.postValue(ApiResponse.success(response.body()?.data))
                    } else {
                        balanceCatalogLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()?.data
                            )
                        )
                    }
                }
            })

        return balanceCatalogLiveData
    }

    override fun getPaymentMethod(authToken: String): LiveData<ApiResponse<List<PaymentMethodResponse>>> {
        val paymentMethodLiveData = MutableLiveData<ApiResponse<List<PaymentMethodResponse>>>()

        apiService.getPaymentMethod(authToken)
            .enqueue(object : Callback<BaseResponse<List<PaymentMethodResponse>>?> {
                override fun onFailure(
                    call: Call<BaseResponse<List<PaymentMethodResponse>>?>,
                    t: Throwable
                ) {
                    paymentMethodLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<BaseResponse<List<PaymentMethodResponse>>?>,
                    response: Response<BaseResponse<List<PaymentMethodResponse>>?>
                ) {
                    if (response.isSuccessful) {
                        paymentMethodLiveData.postValue(ApiResponse.success(response.body()?.data))
                    } else {
                        paymentMethodLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()?.data
                            )
                        )
                    }
                }
            })

        return paymentMethodLiveData
    }

    override fun getTopUpHistory(authToken: String): LiveData<ApiResponse<List<TopUpHistoryResponse>>> {
        val topUpHistoryLiveData = MutableLiveData<ApiResponse<List<TopUpHistoryResponse>>>()

        apiService.getTopUpHistory(authToken)
            .enqueue(object : Callback<BaseResponse<List<TopUpHistoryResponse>>?> {
                override fun onFailure(
                    call: Call<BaseResponse<List<TopUpHistoryResponse>>?>,
                    t: Throwable
                ) {
                    topUpHistoryLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<BaseResponse<List<TopUpHistoryResponse>>?>,
                    response: Response<BaseResponse<List<TopUpHistoryResponse>>?>
                ) {
                    if (response.isSuccessful) {
                        topUpHistoryLiveData.postValue(ApiResponse.success(response.body()?.data))
                    } else {
                        topUpHistoryLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()?.data
                            )
                        )
                    }
                }
            })

        return topUpHistoryLiveData
    }

    override fun postTopUpBalance(
        authToken: String,
        topUpBalanceRequest: TopUpBalanceRequest
    ): LiveData<ApiResponse<TopUpBalanceResponse>> {
        val topUpBalanceLiveData = MutableLiveData<ApiResponse<TopUpBalanceResponse>>()

        apiService.postTopUpBalance(authToken, topUpBalanceRequest)
            .enqueue(object : Callback<BaseResponse<TopUpBalanceResponse>?> {
                override fun onFailure(
                    call: Call<BaseResponse<TopUpBalanceResponse>?>,
                    t: Throwable
                ) {
                    topUpBalanceLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<BaseResponse<TopUpBalanceResponse>?>,
                    response: Response<BaseResponse<TopUpBalanceResponse>?>
                ) {
                    if (response.isSuccessful) {
                        topUpBalanceLiveData.postValue(ApiResponse.success(response.body()?.data))
                    } else
                        topUpBalanceLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()), //response.message(),
                                response.body()?.data
                            )
                        )
                }
            })

        return topUpBalanceLiveData
    }

    override fun postTransferReceipt(
        authToken: String,
        uploadTransferRequest: UploadTransferRequest,
        paymentToken: String
    ): LiveData<ApiResponse<NonBodyResponse>> {
        val transferReceiptLiveData = MutableLiveData<ApiResponse<NonBodyResponse>>()

        val requestFile =
            RequestBody.create(
                MediaType.parse("multipart/form-data"),
                uploadTransferRequest.transferReceipt
            )

        val body = MultipartBody.Part.createFormData(
            "transfer_receipt",
            uploadTransferRequest.transferReceipt.name,
            requestFile
        )

        apiService.postTransferReceipt(authToken, body, paymentToken)
            .enqueue(object : Callback<NonBodyResponse?> {
                override fun onFailure(call: Call<NonBodyResponse?>, t: Throwable) {
                    transferReceiptLiveData.postValue(ApiResponse.error(t.message, null))
                }

                override fun onResponse(
                    call: Call<NonBodyResponse?>,
                    response: Response<NonBodyResponse?>
                ) {
                    if (response.isSuccessful) {
                        transferReceiptLiveData.postValue(ApiResponse.success(response.body()))
                    } else {
                        transferReceiptLiveData.postValue(
                            ApiResponse.empty(
                                getApiErrorMessage(response.code(),response.errorBody()),
                                response.body()
                            )
                        )
                    }
                }
            })

        return transferReceiptLiveData
    }
}