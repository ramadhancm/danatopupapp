package com.dana.batch2.danatopupapp.di

import com.dana.batch2.danatopupapp.ui.confirmqrcode.ConfirmQrCodeActivity
import com.dana.batch2.danatopupapp.ui.confirmreceipt.ConfirmReceiptActivity
import com.dana.batch2.danatopupapp.ui.forgotpassword.EmailVerificationActivity
import com.dana.batch2.danatopupapp.ui.history.FinishedHistoryFragment
import com.dana.batch2.danatopupapp.ui.history.HistoryFragment
import com.dana.batch2.danatopupapp.ui.history.InProgressHistoryFragment
import com.dana.batch2.danatopupapp.ui.login.LoginActivity
import com.dana.batch2.danatopupapp.ui.newhome.NewHomeFragment
import com.dana.batch2.danatopupapp.ui.otp.OtpActivity
import com.dana.batch2.danatopupapp.ui.paymenttype.PaymentTypeActivity
import com.dana.batch2.danatopupapp.ui.signup.SignUpActivity
import com.dana.batch2.danatopupapp.ui.topup.TopUpActivity
import com.dana.batch2.danatopupapp.ui.user.UserFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class,
    RepositoryModule::class, ViewModelModule::class])
interface AppComponent {

    fun inject(target: TopUpActivity)

    fun inject(target: NewHomeFragment)

    fun inject(target: HistoryFragment)

    fun inject(target: UserFragment)

    fun inject(target: PaymentTypeActivity)

    fun inject(target: OtpActivity)

    fun inject(target: LoginActivity)

    fun inject(target: SignUpActivity)

    fun inject(target: ConfirmReceiptActivity)

    fun inject(target: ConfirmQrCodeActivity)

    fun inject(target: FinishedHistoryFragment)

    fun inject(target: InProgressHistoryFragment)

    fun inject(target: EmailVerificationActivity)

}