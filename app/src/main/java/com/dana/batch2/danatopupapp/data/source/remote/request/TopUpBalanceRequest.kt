package com.dana.batch2.danatopupapp.data.source.remote.request

import com.google.gson.annotations.SerializedName

data class TopUpBalanceRequest(
    @SerializedName("phone_number")
    var phoneNumber: String,

    @SerializedName("code")
    var code: String,

    @SerializedName("payment_method_id")
    var idPaymentMethod: Int
)