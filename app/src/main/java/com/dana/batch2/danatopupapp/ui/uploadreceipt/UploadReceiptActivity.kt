package com.dana.batch2.danatopupapp.ui.uploadreceipt

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Matrix
import android.os.Bundle
import android.view.Surface
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.data.source.remote.request.UploadTransferRequest
import com.dana.batch2.danatopupapp.databinding.ActivityUploadReceiptBinding
import com.dana.batch2.danatopupapp.utils.createDirectoryIfNotExist
import com.dana.batch2.danatopupapp.utils.createFile
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.utils.visible
import kotlinx.android.synthetic.main.activity_upload_receipt.*
import java.io.File
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class UploadReceiptActivity : AppCompatActivity() {

    private val executor: Executor by lazy { Executors.newSingleThreadExecutor() }
    private var imageCapture: ImageCapture? = null

    private var lensFacing = CameraX.LensFacing.BACK
    private lateinit var binding: ActivityUploadReceiptBinding

    companion object {
        const val RESULT_TAKE_PHOTO = 202
        const val EXTRA_PHOTO = "extra_photo"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUploadReceiptBinding.inflate(layoutInflater)
        setContentView(binding.rootView)

        requestPermissions()
        setOnClickListener()

    }

    private fun getMetadata() = ImageCapture.Metadata().apply {
        isReversedHorizontal = lensFacing == CameraX.LensFacing.FRONT
    }

    private fun requestPermissions() {
        if (allPermissionsGranted()) {
            previewView.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
    }

    private fun setOnClickListener() {
        onClickClose()
        onClickTakePicture()
    }


    private fun disableActions() {
        previewView.isClickable = false
        takenImage.isClickable = false

    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                previewView.post { startCamera() }
            } else {
                finish()
            }
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }


    private fun startCamera() {
        CameraX.unbindAll()

        val preview = createPreviewUseCase()

        preview.setOnPreviewOutputUpdateListener {

            val parent = previewView.parent as ViewGroup
            parent.removeView(previewView)
            parent.addView(previewView, 0)

            previewView.surfaceTexture = it.surfaceTexture

            updateTransform()
        }

        imageCapture = createCaptureUseCase()

        CameraX.bindToLifecycle(this, preview, imageCapture)
    }

    private fun createPreviewUseCase(): Preview {
        val previewConfig = PreviewConfig.Builder().apply {
            setLensFacing(lensFacing)

            setTargetRotation(previewView.display.rotation)
        }.build()

        return Preview(previewConfig)
    }

    private fun updateTransform() {
        val matrix = Matrix()

        val centerX = previewView.width / 2f
        val centerY = previewView.height / 2f

        val rotationDegrees = when (previewView.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }

        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)
        previewView.setTransform(matrix)
    }

    private fun createCaptureUseCase(): ImageCapture {

        val imageCaptureConfig = ImageCaptureConfig.Builder().apply {
            setLensFacing(lensFacing)
            setTargetRotation(previewView.display.rotation)

            setCaptureMode(ImageCapture.CaptureMode.MAX_QUALITY)
        }

        return ImageCapture(imageCaptureConfig.build())
    }

    private fun onClickClose() {
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    private fun onClickTakePicture() {
        img_take_photo.setOnClickListener { takePicture() }
    }

    private fun takePicture() {
        disableActions()
        savePictureToFile()
    }

    private fun savePictureToFile() {
        binding.progressBarTakePicture.visible()
        createDirectoryIfNotExist()
        val file = createFile()

        imageCapture?.takePicture(file, getMetadata(), executor,
                object : ImageCapture.OnImageSavedListener {
                    override fun onImageSaved(file: File) {

                        runOnUiThread {
                            takenImage.setImageURI(
                                FileProvider.getUriForFile(
                                    this@UploadReceiptActivity,
                                    packageName, file
                                )
                            )
                        }

                        val intent = Intent()
                        intent.putExtra(EXTRA_PHOTO, UploadTransferRequest(file))
                        setResult(RESULT_TAKE_PHOTO, intent)
                        binding.progressBarTakePicture.invisible()
                        finish()
                    }

                    override fun onError(
                        imageCaptureError: ImageCapture.ImageCaptureError,
                        message: String,
                        cause: Throwable?
                    ) {
                        runOnUiThread {
                            binding.progressBarTakePicture.invisible()
                            Toast.makeText(
                                this@UploadReceiptActivity,
                                getString(R.string.image_capture_failed),
                                Toast.LENGTH_SHORT
                            ).show() }
                    }
                })
    }
}
