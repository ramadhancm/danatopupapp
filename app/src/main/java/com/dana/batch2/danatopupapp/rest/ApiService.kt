package com.dana.batch2.danatopupapp.rest

import com.dana.batch2.danatopupapp.data.source.remote.request.*
import com.dana.batch2.danatopupapp.data.source.remote.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST("/login")
    fun postLogin(
        @Body loginRequest: LoginRequest
    ): Call<BaseResponse<LoginResponse>>

    @POST("/sign-up")
    fun postSignUp(
        @Body signUpRequest: SignUpRequest
    ): Call<NonBodyResponse>

    @POST("/resend-otp")
    fun postSendOtp(
        @Body sendOtpRequest: SendOtpRequest
    ): Call<NonBodyResponse>

    @POST("/verify-otp")
    fun postVerifyOtp(
        @Body verifyOtpRequest: VerifyOtpRequest
    ): Call<NonBodyResponse>

    @GET("/get-user-profile")
    fun getUserProfile(
        @Header("Authorization") authToken: String
    ): Call<BaseResponse<UserProfileResponse>>

    @GET("/get-user-balance")
    fun getUserBalance(
        @Header("Authorization") authToken: String
    ): Call<UserBalanceResponse>

    @GET("/get-balance-catalog")
    fun getBalanceCatalog(
        @Header("Authorization") authToken: String
    ): Call<BaseResponse<List<BalanceCatalogResponse>>>

    @GET("/get-payment-method")
    fun getPaymentMethod(
        @Header("Authorization") authToken: String
    ): Call<BaseResponse<List<PaymentMethodResponse>>>

    @GET("/get-topup-history")
    fun getTopUpHistory(
        @Header("Authorization") authToken: String
    ): Call<BaseResponse<List<TopUpHistoryResponse>>>

    @POST("/topup-balance")
    fun postTopUpBalance(
        @Header("Authorization") authToken: String,
        @Body topUpBalanceRequest: TopUpBalanceRequest
    ): Call<BaseResponse<TopUpBalanceResponse>>

    @Multipart
    @POST("/upload-transfer-receipt/{tokenPayment}")
    fun postTransferReceipt(
        @Header("Authorization") authToken: String,
        @Part image: MultipartBody.Part,
        @Path("tokenPayment") paymentToken: String
    ): Call<NonBodyResponse>
}