package com.dana.batch2.danatopupapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.ui.confirmqrcode.ConfirmQrCodeViewModel
import com.dana.batch2.danatopupapp.ui.confirmreceipt.ConfirmReceiptViewModel
import com.dana.batch2.danatopupapp.ui.history.HistoryViewModel
import com.dana.batch2.danatopupapp.ui.login.LoginViewModel
import com.dana.batch2.danatopupapp.ui.newhome.NewHomeViewModel
import com.dana.batch2.danatopupapp.ui.otp.OtpViewModel
import com.dana.batch2.danatopupapp.ui.paymenttype.PaymentTypeViewModel
import com.dana.batch2.danatopupapp.ui.signup.SignUpViewModel
import com.dana.batch2.danatopupapp.ui.topup.TopUpViewModel
import com.dana.batch2.danatopupapp.ui.user.UserViewModel
import javax.inject.Inject

class ViewModelFactory @Inject constructor(private val mainRepository: MainRepository) :
    ViewModelProvider.NewInstanceFactory() {


    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> LoginViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(SignUpViewModel::class.java) -> SignUpViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(TopUpViewModel::class.java) -> TopUpViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(HistoryViewModel::class.java) -> HistoryViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(UserViewModel::class.java) -> UserViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(PaymentTypeViewModel::class.java) -> PaymentTypeViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(OtpViewModel::class.java) -> OtpViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(ConfirmReceiptViewModel::class.java) -> ConfirmReceiptViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(ConfirmQrCodeViewModel::class.java) -> ConfirmQrCodeViewModel(
                mainRepository
            ) as T

            modelClass.isAssignableFrom(NewHomeViewModel::class.java) -> NewHomeViewModel(
                mainRepository
            ) as T

            else -> throw IllegalArgumentException("Unknown ViewModel Class ${modelClass::class.java.simpleName}")
        }
    }
}