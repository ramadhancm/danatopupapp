package com.dana.batch2.danatopupapp.ui.history

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpHistoryResponse
import com.dana.batch2.danatopupapp.utils.convertStatusCode
import com.dana.batch2.danatopupapp.utils.formatNominalBalance
import com.dana.batch2.danatopupapp.utils.getPaymentType

class HistoryAdapter: RecyclerView.Adapter<HistoryAdapter.HistoryHolder>() {

    private var history = mutableListOf<TopUpHistoryResponse>()
    private lateinit var listener: (TopUpHistoryResponse) -> Unit

    fun setOnClickHistory(listener: (TopUpHistoryResponse) -> Unit){
        this.listener = listener
    }

    fun setHistory(listHistory: List<TopUpHistoryResponse>){
        history.clear()
        history.addAll(listHistory)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryHolder {
        return HistoryHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false))
    }

    override fun getItemCount(): Int {
        return history.size
    }

    override fun onBindViewHolder(holder: HistoryHolder, position: Int) {
        holder.bindItems(history[position], listener)
    }

    inner class HistoryHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bindItems(
            history: TopUpHistoryResponse,
            listener: (TopUpHistoryResponse) -> Unit
        ){
            val paymentDate: TextView = itemView.findViewById(R.id.textPaymentDate)
            val transactionType: TextView = itemView.findViewById(R.id.textTransactionType)
            val nominal: TextView = itemView.findViewById(R.id.textNominal)
            val invoiceId: TextView = itemView.findViewById(R.id.textInvoiceId)
            val transactionStatus: TextView = itemView.findViewById(R.id.textTransactionStatus)

            val transactionId = "Id: ${history.invoiceId}"

            paymentDate.text = history.createdAt
            transactionType.text = getPaymentType(history.paymentType)
            nominal.text = formatNominalBalance(history.topUpBalance.toInt())
            invoiceId.text = transactionId
            transactionStatus.text = convertStatusCode(history.status)

            val cardHistory: CardView = itemView.findViewById(R.id.cardHistory)

            cardHistory.setOnClickListener{
                listener(history)
            }
        }
    }
}