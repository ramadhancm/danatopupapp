package com.dana.batch2.danatopupapp.data.source.remote.response

import com.google.gson.annotations.SerializedName

data class UserBalanceResponse(
    @SerializedName("amount")
    var amount: Float
)