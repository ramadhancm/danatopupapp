package com.dana.batch2.danatopupapp.ui.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.dana.batch2.danatopupapp.data.source.MainRepository
import com.dana.batch2.danatopupapp.data.source.remote.ApiResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.TopUpHistoryResponse


class HistoryViewModel(private val mainRepository: MainRepository) : ViewModel() {

    private var authToken = ""
    fun setAuthToken(token: String) {
        authToken = token
    }

    fun getTopUpHistory(): LiveData<ApiResponse<List<TopUpHistoryResponse>>> {
        return mainRepository.getTopUpHistory(authToken)
    }

    fun isPendingTopUpEmpty(topUpHistory: List<TopUpHistoryResponse>, pendingCode: Int): Boolean {
        return  topUpHistory.filter { it.status == pendingCode }.count()  < 1
    }

    fun isFinishedTopUpEmpty(topUpHistory: List<TopUpHistoryResponse>, pendingCode: Int): Boolean {
        return  topUpHistory.filter { it.status != pendingCode }.count() < 1
    }
}