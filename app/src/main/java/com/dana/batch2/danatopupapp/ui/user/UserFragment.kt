package com.dana.batch2.danatopupapp.ui.user

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.dana.batch2.danatopupapp.BuildConfig
import com.dana.batch2.danatopupapp.R
import com.dana.batch2.danatopupapp.application.DanaTopUpApplication
import com.dana.batch2.danatopupapp.data.source.remote.StatusResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserBalanceResponse
import com.dana.batch2.danatopupapp.data.source.remote.response.UserProfileResponse
import com.dana.batch2.danatopupapp.ui.login.LoginActivity
import com.dana.batch2.danatopupapp.utils.formatNominalBalance
import com.dana.batch2.danatopupapp.utils.invisible
import com.dana.batch2.danatopupapp.utils.visible
import com.dana.batch2.danatopupapp.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_user.*
import javax.inject.Inject

class UserFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var userViewModel: UserViewModel
    private lateinit var preference: SharedPreferences
    private lateinit var authTokenPref: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        init()
        //put your code here
        onClickListener()
    }

    private fun init() {
        injectViewModelFactory()
        initViewModel()
        initSharedPreference()
        setAuthToken()
        getUserProfile()
        getUserBalance()
        progress_bar.visible()
    }

    private fun injectViewModelFactory() {
        (requireActivity().application as DanaTopUpApplication)
            .dataTopUpApplication.inject(this@UserFragment)
    }

    private fun initViewModel() {
        userViewModel =
            ViewModelProvider(this@UserFragment, viewModelFactory).get(UserViewModel::class.java)
    }

    private fun initSharedPreference() {
        preference = requireActivity().getSharedPreferences(BuildConfig.PREF, 0)
        authTokenPref = preference.getString(BuildConfig.AUTH_TOKEN, "") ?: ""
    }

    private fun setAuthToken() {
        userViewModel.setAuthToken(authTokenPref)
    }

    private fun loadUserProfile(userProfile: UserProfileResponse) {
        val name = "${userProfile.firstName} ${userProfile.lastName}"
        textName.text = name
        textEmail.text = userProfile.email
        textPhoneNumber.text = userProfile.phoneNumber
    }

    private fun loadUserBalance(userBalance: UserBalanceResponse) {
        textBalanceNominal.text =
            formatNominalBalance(userBalance.amount.toInt())
    }

    private fun getUserProfile() {
        userViewModel.getUserProfile().observe(viewLifecycleOwner, Observer { apiResponse ->
            when (apiResponse.status) {
                StatusResponse.SUCCESS -> {
                    apiResponse?.body?.let {
                        loadUserProfile(it)
                        progress_bar.invisible()
                    }
                }

                StatusResponse.EMPTY -> {
                    showErrorMessage()
                    progress_bar.invisible()
                }

                StatusResponse.ERROR -> {
                    showErrorMessage()
                    progress_bar.invisible()
                }
            }
        })
    }

    private fun getUserBalance() {
        userViewModel.getUserBalance().observe(viewLifecycleOwner, Observer { apiResponse ->
            when (apiResponse.status) {
                StatusResponse.SUCCESS -> {
                    apiResponse?.body?.let {
                        loadUserBalance(it)
                    }
                }
                StatusResponse.EMPTY -> {
                    showErrorMessage()
                }
                StatusResponse.ERROR -> {
                    showErrorMessage()
                }
            }
        })
    }

    private fun onClickListener() {
        img_logout.setOnClickListener {
            showAlertLogout()
        }
    }

    private fun showAlertLogout() {
        context?.let { fragmentContext ->
            AlertDialog.Builder(fragmentContext).apply {
                setMessage(getString(R.string.are_sure_logout))
                setCancelable(false)
                setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.cancel() }
                setPositiveButton(getString(R.string.yes)) { _, _ -> loggingOut() }
                create()
                show()
            }
        }
    }

    private fun loggingOut() {
        val editPref = preference.edit()
        editPref.putString(BuildConfig.AUTH_TOKEN, "")
        editPref.apply()
        startActivity(Intent(activity, LoginActivity::class.java))
        activity?.finish()
    }

    private fun showErrorMessage() {
        Toast.makeText(
            context, getString(R.string.bad_internet),
            Toast.LENGTH_SHORT
        ).show()
    }
}
